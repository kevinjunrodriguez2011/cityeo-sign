// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  tenant: 'tiplb2c.onmicrosoft.com',
  tenantName: 'tiplb2c',
  clientID: 'a198d067-29ab-405b-abad-3d8d636a5182',
  signUpSignInPolicy: 'B2C_1A_signup_signin',
  passwordResetPolicy: 'B2C_1A_PasswordReset',
  b2cScopes: ['https://tiplb2c.onmicrosoft.com/digisignapi/read'],
  extraB2cScopes: ['https://tiplb2c.onmicrosoft.com/digisignapi/write'],
  webApi: 'https://localhost:44386/api',
  signalRHubUrl: 'https://localhost:44386',
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
