export const environment = {
  production: true,
  tenant: 'tiplb2c.onmicrosoft.com',
  tenantName: 'tiplb2c',
  clientID: 'a198d067-29ab-405b-abad-3d8d636a5182',
  signUpSignInPolicy: 'B2C_1_SignInSignUp',
  passwordResetPolicy: 'B2C_1A_PasswordReset',
  b2cScopes: ['https://tiplb2c.onmicrosoft.com/digisignapi/read'],
  extraB2cScopes: ['https://tiplb2c.onmicrosoft.com/digisignapi/write'],
  webApi: 'https://digi-sign-api.azurewebsites.net/api',
  signalRHubUrl: 'https://digi-sign-api.azurewebsites.net',
};
