import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { NotifyEventService } from '../../../services/notify-event.service';
import { MediaLibraryService } from 'src/app/services/media-library.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { MediaLibrary } from '../../../models/MediaLibrary';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { PlaylistService } from '../../../services/playlist.service';

@Component({
  selector: 'app-control-panel-modal',
  templateUrl: './control-panel-modal.component.html',
  styleUrls: ['./control-panel-modal.component.css']
})
export class ControlPanelModalComponent implements OnInit {
  browserType: string;
  os: string;
  mediaLibraryTypes = [];
  defaultMediaLibraryType = {
    Id: 0,
    Name: 'All',
    Selected: true
  };
  selectedMediaLibraryTypeId: number = this.defaultMediaLibraryType.Id;
  TotalRecords = 0;
  PageStart = 0;
  PageSize = 25;
  mediaLibraries: Array<MediaLibrary> = [];
  scrollDistance = 1;
  throttle = 300;
  isLoadMore: boolean = false;
  searchTerm: string;
  isLoading: boolean = false;
  selectedItem: { Id: number, Idx: number, mediaLibrary: MediaLibrary };

  constructor(public bsModalRef: BsModalRef,
    public playlistService: PlaylistService,
    private notifyEventService: NotifyEventService,
    private mediaLibraryService: MediaLibraryService,
    public commonFunctionsService: CommonFunctionsService) {
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os')
  }

  ngOnInit() {
    this.notifyEventService.mediaLibrarySearchTermEvent$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((term: string) => {
        this.PageStart = 0;
        this.searchTerm = term;
        this.mediaLibraries = [];

        return this.mediaLibraryService
          .GetMediaLibraryList(this.selectedMediaLibraryTypeId, this.PageStart, this.PageSize, this.searchTerm)
      })
    ).subscribe(res =>
      this.onReceiveData(res)
    );

    this.mediaLibraryService.GetMediaLibraryTypeList().subscribe((res: any) => {
      this.mediaLibraryTypes = res;
      //add default dropdown option for media library types
      this.mediaLibraryTypes.unshift(this.defaultMediaLibraryType);
    });
    this.getMediaLibraryList(true);
  }

  getMediaLibraryList(isPageLoad: boolean = false) {
    this.isLoading = true;
    this.mediaLibraryService
      .GetMediaLibraryList(this.selectedMediaLibraryTypeId, this.PageStart, this.PageSize, this.searchTerm)
      .subscribe((res: any) => this.onReceiveData(res, isPageLoad), 
        (error: any)=>{
        this.isLoading = false;
      });
  }

  onReceiveData(res: any, isPageLoad: boolean = false) {
    this.isLoading = false;
    this.TotalRecords = res.TotalCount;
    if (res.MediaLibraryList.length > 0) {
      res.MediaLibraryList.forEach(element => {
        element.CreatedDatetime = new Date(element.CreatedDatetime + 'Z');

        if (element.Duration == null)
          element.Duration = this.commonFunctionsService.defaultMediaDuration;

        element.Selected = false;
      });

      let localFilteredMedias = [];
      //logic to put all selected items at the top of the list on first page only
      //(this is done because some item(s) may not exist(s) in current page)
      if (this.PageStart === 0) {
        //add received item from another component on page load only
        if (isPageLoad
          && this.selectedItem.mediaLibrary !== undefined && this.selectedItem.mediaLibrary !== null && this.selectedItem.Idx !== undefined) {
          this.playlistService.selectMedia(this.selectedItem.mediaLibrary);
        }

        //check if any selected media found
        if (this.playlistService.selectedMedias.length > 0) {
          localFilteredMedias = this.playlistService.selectedMedias.filter(x =>
            (
              (this.searchTerm === undefined || this.searchTerm == null || this.searchTerm === '')
              || (x.Title.indexOf(this.searchTerm) > -1)
            )
            &&
            (this.selectedMediaLibraryTypeId === 0 || this.selectedMediaLibraryTypeId === x.MediaLibraryTypeId)
          );

          if (localFilteredMedias != null && localFilteredMedias.length > 0) {
            localFilteredMedias.forEach(media => {
              this.mediaLibraries.push(media);
            })
          }
        }
      }

      //remove existing items from the received api results to avoid duplication
      if (this.playlistService.selectedMedias != null && this.playlistService.selectedMedias.length > 0) {
        this.playlistService.selectedMedias.forEach(media => {

          let index = res.MediaLibraryList.findIndex(x => x.Id == media.Id);
          if (index > -1)
            res.MediaLibraryList.splice(index, 1);
        })
      }

      this.mediaLibraries = this.mediaLibraries.concat(res.MediaLibraryList);
    }

    this.isLoadMore = this.mediaLibraries.length < this.TotalRecords;
  }

  changeMediaLibraryType(mediaLibraryType) {
    if (this.selectedMediaLibraryTypeId !== mediaLibraryType.Id) {
      this.selectedMediaLibraryTypeId = mediaLibraryType.Id;

      this.mediaLibraryTypes.forEach(x => x.Selected = false);
      mediaLibraryType.Selected = true;

      this.PageStart = 0;
      this.mediaLibraries = [];
      this.getMediaLibraryList();
    }
  }

  searchMediaLibrary() {
    if (this.searchTerm !== undefined
      && !(this.searchTerm.length > 0 && this.searchTerm.trim().length == 0))
      this.notifyEventService.onMediaLibrarySearchTerm(this.searchTerm);
  }

  onScrollDown(event) {
    if (this.isLoadMore) {
      this.PageStart++;
      this.getMediaLibraryList();
    }
  }

  addMedias() {
    this.notifyEventService.onModalAddMedia(this.playlistService.selectedMedias);
    this.bsModalRef.hide();
  }

  isVideoType(id: number) {
    let count = this.mediaLibraryTypes.filter(x => x.Id === id && String(x.Name).toLocaleLowerCase().indexOf('video') > -1).length;

    return count > 0;
  }

  onEditMediaDuration(media) {
    media.editMediaDuration = !media.editMediaDuration;
  }

  exitMediaDurationEdit(media) {
    media.editMediaDuration = false;
    if (media.Duration == '' || media.Duration <= 0) {
      media.Duration = this.commonFunctionsService.defaultMediaDuration;
    }
  }
}
