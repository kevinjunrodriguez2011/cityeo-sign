import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlPanelModalComponent } from '../modal-templates/control-panel-modal/control-panel-modal.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng-click-outside';
import { PlaylistPreviewComponent } from './playlist-preview/playlist-preview.component';
import { PlaylistScheduleModalComponent } from './playlist-schedule-modal/playlist-schedule-modal.component';
import { PlayerSetupModalComponent } from './player-setup-modal/player-setup-modal.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    InfiniteScrollModule,
    FormsModule,
    ClickOutsideModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ControlPanelModalComponent, PlaylistPreviewComponent, PlaylistScheduleModalComponent, PlayerSetupModalComponent],
  exports: [ControlPanelModalComponent, PlaylistPreviewComponent, PlaylistScheduleModalComponent, PlayerSetupModalComponent]
})
export class ModalTemplatesModule { }
