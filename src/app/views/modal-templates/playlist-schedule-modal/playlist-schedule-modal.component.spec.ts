import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistScheduleModalComponent } from './playlist-schedule-modal.component';

describe('PlaylistScheduleModalComponent', () => {
  let component: PlaylistScheduleModalComponent;
  let fixture: ComponentFixture<PlaylistScheduleModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistScheduleModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistScheduleModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
