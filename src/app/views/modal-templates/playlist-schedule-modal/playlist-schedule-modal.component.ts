import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { PlaylistService } from 'src/app/services/playlist.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { NotifyEventService } from 'src/app/services/notify-event.service';
import { ApiError } from 'src/app/models/ApiError';
import { BsModalRef } from 'ngx-bootstrap';
import { IPlaylistList } from 'src/app/interfaces/IPlaylistList';

@Component({
  selector: 'app-playlist-schedule-modal',
  templateUrl: './playlist-schedule-modal.component.html',
  styleUrls: ['./playlist-schedule-modal.component.css']
})
export class PlaylistScheduleModalComponent implements OnInit {
  playlistId: number;
  scheduleModalForm: FormGroup;
  playlist: IPlaylistList;
  scheduleFormGroupKey = 'Schedule';

  get IsEveryday() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' +  'IsEveryday'); }
  get IsForFullDay() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'IsForFullDay'); }
  get TheseDays() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'TheseDays'); }
  get ThisTime() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'ThisTime'); }
  get TheseDaysCheckBoxGroup() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'TheseDaysCheckBoxGroup') as FormArray; }
  get StartTime() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'StartTime'); }
  get EndTime() { return this.scheduleModalForm.get(this.scheduleFormGroupKey + '.' + 'EndTime'); }
  
  constructor(public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private playlistService: PlaylistService,
    private commonFunctionsService: CommonFunctionsService,
    private notifyEventService: NotifyEventService) {}

  ngOnInit() {
    this.scheduleModalForm = this.formBuilder.group({});
  }

  formInitialized(name: string, form: FormGroup) {
    this.scheduleModalForm.setControl(name, form);
  }

  setPlayListSchedule() {
    if (this.scheduleModalForm.valid) {
      let playlistScheduleSaveModel: any = {};

      let selectedDays = this.TheseDaysCheckBoxGroup.controls
        .filter(control => control.value.Checked)
        .map(days =>
          days.value.Id
        );

      playlistScheduleSaveModel.UserPlaylistId = this.playlist.Id;
      playlistScheduleSaveModel.IsEveryday = this.IsEveryday.value;
      playlistScheduleSaveModel.Days = selectedDays.length > 0 ? selectedDays.join(','): null;
      playlistScheduleSaveModel.IsForFullDay = this.IsForFullDay.value;
      playlistScheduleSaveModel.StartTime = this.StartTime.value;
      playlistScheduleSaveModel.EndTime = this.EndTime.value;

      this.playlistService.SetPlayListSchedule(playlistScheduleSaveModel).subscribe((res: any) => {
        this.commonFunctionsService.displaySuccessMessage('Playlist schedule saved successfully!');
        this.notifyEventService.onPlaylistSetSchedule(res.Id);
        this.bsModalRef.hide();
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
    }
  }

}
