import { Component, OnInit, ElementRef, ViewChild, AfterViewChecked, ViewEncapsulation, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { BsModalRef } from 'ngx-bootstrap';
import { MediaLibrary } from 'src/app/models/MediaLibrary';
import { MediaLibraryTypeEnum } from 'src/app/models/MediaLibraryTypeEnum';
import { of, Subject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-preview',
  templateUrl: './playlist-preview.component.html',
  styleUrls: ['./playlist-preview.component.css'],
})
export class PlaylistPreviewComponent implements OnInit, OnDestroy, AfterViewChecked {
  ngUnsubscribe: Subject<any> = new Subject<any>();
  ImageMediaLibraryType: MediaLibraryTypeEnum = MediaLibraryTypeEnum.Image;
  VideoMediaLibraryType: MediaLibraryTypeEnum = MediaLibraryTypeEnum.Video;
  playlistMedia: MediaLibrary[];
  isPortrait: boolean = false;
  defaultHeightWidth: number = 400;
  frameWidth: number;
  frameHeight: number;
  finalWidth: number;
  finalHeight: number;
  currentIndex: number = 0;
  isComponentActive: boolean;
  get currentInterval() {
    return this.playlistMedia.length > 0 && this.currentIndex < this.playlistMedia.length ?
      this.playlistMedia[this.currentIndex].Duration * 1000
      : 0;
  };
  private videoElement: ElementRef;

  @ViewChild('videoElement') set videoElementContent(content: ElementRef) {
    this.videoElement = content;
  }

  constructor(public bsModalRef: BsModalRef, private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit() {
    if(this.frameWidth === undefined)
      this.frameWidth = this.defaultHeightWidth;
    
    if(this.frameHeight === undefined)
      this.frameHeight = this.defaultHeightWidth;

    this.finalHeight = this.defaultHeightWidth;
    this.finalWidth = this.frameWidth / this.frameHeight * this.finalHeight;

    if(this.isPortrait){
      if(this.finalWidth > this.finalHeight){
       let temp = this.finalWidth;
       this.finalWidth =  this.finalHeight;
       this.finalHeight = temp;
      }
    }
    else{
      if(this.finalWidth < this.finalHeight){
        let temp = this.finalWidth;
        this.finalWidth = this.finalHeight;
        this.finalHeight = temp;
       }
    }

    if(this.finalHeight > this.defaultHeightWidth){
      this.finalWidth = this.finalWidth /  this.finalHeight * this.defaultHeightWidth
      this.finalHeight = this.defaultHeightWidth;
    }

    if(this.playlistMedia !=undefined && this.playlistMedia !=null && this.playlistMedia.length > 0){
      this.isComponentActive = true;
      this.resetPlaylist();
    }
  }

  ngOnDestroy() {
    this.isComponentActive = false;
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngAfterViewChecked() {
    if (this.videoElement != undefined) {
      if (this.videoElement.nativeElement.paused)
        this.videoElement.nativeElement.play();

      this.videoElement.nativeElement.onended = () => {
        //this.slidingInterval = setInterval(this.rotateMedia, 1000);
        this.playlistMedia[this.currentIndex].Selected = false;
        this.goToNextMedia();
      };
    }
  }

  rotateMedia() {
    if(!this.isComponentActive)
      return;

    if (this.currentIndex < this.playlistMedia.length) {
      this.playlistMedia[this.currentIndex].Selected = true;
      if (!this.changeDetectorRef['destroyed']) {
        this.changeDetectorRef.detectChanges();
      }
      if (this.playlistMedia[this.currentIndex].MediaLibraryTypeId === this.ImageMediaLibraryType) {
        //images
        let observable = of(true)
          .pipe(takeUntil(this.ngUnsubscribe), delay(this.currentInterval));

        observable.subscribe(res => {
          this.playlistMedia[this.currentIndex].Selected = false;
          if (!this.changeDetectorRef['destroyed']) {
            this.changeDetectorRef.detectChanges();
          }
          this.goToNextMedia();
        });
      }
      else {
        //videos
        //video handling done in after view checked event
      }
    }
    else {
      //playlist rotated fully once

      //to rotate playlist again from the start
      this.resetPlaylist();
    }
  }

  resetPlaylist(){
    this.playlistMedia.forEach(media=>{
      media.Selected = false;
    })
    if (!this.changeDetectorRef['destroyed']) {
      this.changeDetectorRef.detectChanges();
    }
    this.currentIndex = 0;
    //this.playlistMedia[0].Selected = true;
    this.rotateMedia();
  }

  goToNextMedia(){
    if(!this.isComponentActive)
      return;

    if(this.currentIndex < this.playlistMedia.length)
      this.currentIndex++;

    this.rotateMedia();
  }

}
