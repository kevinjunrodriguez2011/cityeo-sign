import { Component, OnInit, Input } from '@angular/core';
import { NotifyEventService } from 'src/app/services/notify-event.service';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-player-setup-modal',
  templateUrl: './player-setup-modal.component.html',
  styleUrls: ['./player-setup-modal.component.css']
})
export class PlayerSetupModalComponent implements OnInit {
  @Input() controlCenter: any;

  constructor(public bsModalRef: BsModalRef,
    private notifyEventService: NotifyEventService) { }

  ngOnInit() {
    this.notifyEventService.playerSetupEvent$.subscribe(res=>{
      this.bsModalRef.hide();
    })
  }

}
