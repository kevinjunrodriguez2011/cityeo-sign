import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsComponent implements OnInit {

  checkedPermissions = false;
  checkAllPermissions = false;
  showAddUser = false;

  permissionList = [
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Contributor',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
    {
      image: 'https://picsum.photos/59/59',
      name: 'John Apple',
      role: 'Access',
      checked: false
    },
  ]

  constructor() { }

  ngOnInit() {
  }

  checkPermissions(media) {
    this.checkedPermissions = !this.checkedPermissions;
  }
  hasCheckedPermissions() {
    return this.permissionList.filter(m => m.checked).length > 0;
  }
  permissionsCheckAll() {
    this.permissionList.forEach(c => {
      c.checked = this.checkAllPermissions;
    });
  }
  removeAllCheckedPermissions() {
    this.permissionList = this.permissionList.filter(c => !c.checked);
  }
  permissionsCheckedLength() {
    return this.permissionList.filter(m => m.checked).length;
  }
  exitAddUser() {
    this.showAddUser = !this.showAddUser;
  }
  ngAfterViewInit() {

  }
}
