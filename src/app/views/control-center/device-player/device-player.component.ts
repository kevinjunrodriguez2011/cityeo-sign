import { Component, OnInit, Input, ElementRef, ViewChild, ChangeDetectorRef, AfterViewChecked, Output, EventEmitter, OnChanges, SimpleChanges, KeyValueDiffers, KeyValueDiffer, OnDestroy, DoCheck, Optional } from '@angular/core';
import { MediaLibraryTypeEnum } from 'src/app/models/MediaLibraryTypeEnum';
import { IDeviceList } from 'src/app/interfaces/IDeviceList';
import { of, Subject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';
import { SkyhookSortableRenderer } from '@angular-skyhook/sortable';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { DevicesService } from 'src/app/services/devices.service';
import { ControlCenterService } from 'src/app/services/control-center.service';
import { ApiError } from 'src/app/models/ApiError';
import { NotifyEventService } from 'src/app/services/notify-event.service';

@Component({
  selector: 'app-device-player',
  templateUrl: './device-player.component.html',
  styleUrls: ['./device-player.component.css']
})
export class DevicePlayerComponent implements OnChanges, OnInit, OnDestroy, DoCheck, AfterViewChecked {
  @Input() controlCenter: IDeviceList;
  @Input() index: number;
  @Input() isPreview: boolean;
  @Output() openPlayerSetup = new EventEmitter();
  ngUnsubscribe: Subject<any> = new Subject<any>();
  imageMediaLibraryType: MediaLibraryTypeEnum = MediaLibraryTypeEnum.Image;
  videoMediaLibraryType: MediaLibraryTypeEnum = MediaLibraryTypeEnum.Video;
  differ: KeyValueDiffer<string, any>;

  showControlCenterOptions = false;
  showOfflineError = false;
  secondsSinceOffline = 0;
  isExpandedPlaylist = false;
  isComponentActive: boolean;
  currentIndex: number = 0;
  get currentInterval() {
    return this.controlCenter.Playlist.PlayListItems.length > 0 && this.currentIndex < this.controlCenter.Playlist.PlayListItems.length ?
      this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Duration * 1000
      : 0;
  };

  private videoElement: ElementRef;

  @ViewChild('videoElement') set videoElementContent(content: ElementRef) {
    this.videoElement = content;
  }

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private differs: KeyValueDiffers,
    private deviceService: DevicesService,
    private controlCenterService: ControlCenterService,
    private commonFunctionsService: CommonFunctionsService,
    private notifyEventService: NotifyEventService,
    @Optional() public render: SkyhookSortableRenderer<IDeviceList>) {
    this.differ = this.differs.find({}).create();
  }

  ngDoCheck() {
    //differ is used here when object reference has not changed but only some properties have changed
    var changes = this.differ.diff(this.controlCenter);

    if (changes) {
      changes.forEachChangedItem(r => {
        if (r.key === 'IsOnline') {
          if (this.controlCenter != undefined && this.controlCenter != null) {
            this.setDeviceOfflineTime();
            this.startPlaylist();
          }
        }
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    //this is used if object reference has changed
    if (changes.controlCenter) {
      this.isComponentActive = true;

      if (this.controlCenter != undefined && this.controlCenter != null) {
        this.setDeviceOfflineTime();
        this.startPlaylist();
      }
    }
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.isComponentActive = false;
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngAfterViewChecked() {
    if (this.videoElement != undefined) {
      if (this.videoElement.nativeElement.paused)
        this.videoElement.nativeElement.play();

      this.videoElement.nativeElement.onended = () => {
        //this.slidingInterval = setInterval(this.rotateMedia, 1000);
        this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Selected = false;
        this.goToNextMedia();
      };
    }
  }

  setDeviceOfflineTime() {
    if (!this.controlCenter.IsOnline) {
      let currDate = new Date();

      let receivedDate = new Date();

      if (this.controlCenter.OfflineDatetime !== undefined
        && this.controlCenter.OfflineDatetime != null)
        receivedDate = new Date(this.controlCenter.OfflineDatetime + 'Z');

      //to do: this is temporary
      // let receivedDate = new Date();
      // receivedDate.setHours(currDate.getHours() -1);

      let difMilliseconds = receivedDate.getTime() - currDate.getTime();
      this.secondsSinceOffline = Math.round(Math.abs(difMilliseconds / 1000));
    }
    else {
      this.secondsSinceOffline = 0;
    }
  }

  rotateMedia() {
    if (!this.isComponentActive)
      return;

    if (this.currentIndex < this.controlCenter.Playlist.PlayListItems.length) {
      this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Selected = true;
      if (!this.changeDetectorRef['destroyed']) {
        this.changeDetectorRef.detectChanges();
      }
      if (this.controlCenter.IsOnline) {
        //if (this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.MediaLibraryTypeId === this.imageMediaLibraryType) {
        //images
        // setTimeout(() => {
        //   this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Selected = false;
        //   this.changeDetectorRef.detectChanges();
        //   this.goToNextMedia();
        // }, this.currentInterval);

        let observable = of(true)
          .pipe(takeUntil(this.ngUnsubscribe), delay(this.currentInterval));

        observable.subscribe(res => {

          this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Selected = false;
          if (!this.changeDetectorRef['destroyed']) {
            this.changeDetectorRef.detectChanges();
          }
          this.goToNextMedia();
        });
        //}
        //else {
        //videos
        //video handling done in after view checked event
        //}
      }
    }
    else {
      //playlist rotated fully once

      //to rotate playlist again from the start
      this.startPlaylist();
    }
  }

  startPlaylist() {
    if (this.controlCenter.Playlist.PlayListItems != undefined
      && this.controlCenter.Playlist.PlayListItems != null
      && this.controlCenter.Playlist.PlayListItems.length > 0) {
      this.resetPlaylist();
      this.rotateMedia();
    }
  }

  resetPlaylist() {
    this.controlCenter.Playlist.PlayListItems.forEach(media => {
      media.MediaLibrary.Selected = false;
    })
    if (!this.changeDetectorRef['destroyed']) {
      this.changeDetectorRef.detectChanges();
    }
    this.currentIndex = 0;
    this.controlCenter.Playlist.PlayListItems[this.currentIndex].MediaLibrary.Selected = true;
  }


  goToNextMedia() {
    if (!this.isComponentActive)
      return;

    if (this.currentIndex < this.controlCenter.Playlist.PlayListItems.length)
      this.currentIndex++;

    this.rotateMedia();
  }

  editControlCenter(id) {
    this.openPlayerSetup.emit(id);
  }

  togglePlaylist() {
    this.isExpandedPlaylist = !this.isExpandedPlaylist;
  }

  removeControlCenter() {
    if (this.controlCenter != null && this.controlCenter.Id > 0) {
      this.controlCenterService.removeControlCenter(this.controlCenter.Id)
        .subscribe((res: any) => {
          this.commonFunctionsService.displaySuccessMessage('Player Setup details removed successfully!');
          this.showControlCenterOptions = false;
          this.notifyEventService.onPlayerSetup(true);
        }, (error: any) => {
          this.showControlCenterOptions = false;
          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null)
            this.commonFunctionsService.displayErrorMessage(error.error.error_description);
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }

  }

  rebootDevice() {
    this.deviceService.rebootDevice(this.controlCenter.UniqueId)
      .subscribe(res => {
        this.showControlCenterOptions = false;
        this.commonFunctionsService.displaySuccessMessage('Device reboot command sent successfully!');
      }, (error: any) => {
        this.showControlCenterOptions = false;
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }
}
