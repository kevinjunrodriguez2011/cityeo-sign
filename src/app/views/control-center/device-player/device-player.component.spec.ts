import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevicePlayerComponent } from './device-player.component';

describe('DevicePlayerComponent', () => {
  let component: DevicePlayerComponent;
  let fixture: ComponentFixture<DevicePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevicePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevicePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
