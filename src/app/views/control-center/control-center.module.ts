import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { ControlCenterRoutingModule } from './control-center-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlCenterService } from 'src/app/services/control-center.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PopoverModule, BsDropdownModule } from 'ngx-bootstrap';
import { DevicePlayerComponent } from './device-player/device-player.component';
import { SkyhookSortableModule } from '@angular-skyhook/sortable';
import { SkyhookDndModule } from '@angular-skyhook/core';
import { SkyhookMultiBackendModule } from '@angular-skyhook/multi-backend';
import { customMultiBackend } from 'src/app/customMultiBackend';
import { StopwatchComponent } from '../stopwatch/stopwatch.component';
import { SharedModule } from '../shared/shared.module';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  imports: [
    CommonModule,
    ControlCenterRoutingModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    SkyhookSortableModule,
    SkyhookDndModule.forRoot({ backendFactory: customMultiBackend }),
    SkyhookMultiBackendModule,
    PopoverModule,
    ClickOutsideModule,
    BsDropdownModule.forRoot(),
    SharedModule
  ],
  declarations: [IndexComponent, DevicePlayerComponent, StopwatchComponent],
  exports: [IndexComponent, DevicePlayerComponent],
  providers: [ControlCenterService]
})
export class ControlCenterModule { }
