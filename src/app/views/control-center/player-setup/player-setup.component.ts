import { Component, OnInit, AfterViewInit, OnDestroy, SimpleChanges, OnChanges, Input } from '@angular/core';
import { ControlCenterService } from 'src/app/services/control-center.service';
import { PlaylistService } from 'src/app/services/playlist.service';
import { DevicesService } from 'src/app/services/devices.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { forkJoin, Subject } from 'rxjs';
import { FormGroup, FormBuilder, FormArray, ValidatorFn } from '@angular/forms';
import { ApiError } from 'src/app/models/ApiError';
import { trigger, style, animate, transition } from '@angular/animations';
import { takeUntil } from 'rxjs/operators';
import { NotifyEventService } from 'src/app/services/notify-event.service';
import { IDeviceList } from 'src/app/interfaces/IDeviceList';
import { IPlaylistList } from 'src/app/interfaces/IPlaylistList';

@Component({
  selector: 'app-player-setup',
  templateUrl: './player-setup.component.html',
  styleUrls: ['./player-setup.component.css'],
  animations: [
    trigger('SlideToggleRight', [
      transition(':enter', [
        style({ 'z-index': -1, transform: 'translateX(-100%)' }),
        animate('1s ease-in-out', style({ transform: 'translateX(0)' })),
        style({ 'z-index': 0 }),
      ]),
      transition(':leave', [
        style({ 'z-index': -1 }),
        animate('1s ease-in-out', style({ transform: 'translateX(-100%)' }))
      ])
    ])
  ]
})

export class PlayerSetupComponent implements OnChanges, OnInit, AfterViewInit, OnDestroy {
  @Input() controlCenter: any = {};
  playerSetupMenus: any = [];
  displaySizeList: any = [];
  deviceList: IDeviceList[] = [];
  playlistList: IPlaylistList[] = [];

  PlaylistTotalRecords = 0;
  PlaylistPageStart = 0;
  PlaylistPageSize = 25;
  PlaylistIsLoadMore: boolean = false;

  defaultDeviceCheckBox = { Id: -1, Name: 'New Device', Checked: false };
  DeviceTotalRecords = 0;
  DevicePageStart = 0;
  DevicePageSize = 25;
  DeviceIsLoadMore: boolean = false;

  scrollDistance = 1;
  throttle = 300;
  isLoading: boolean = true;
  isDeviceLoading: boolean = true;
  isGuideOpened: boolean = false;

  controlCenterId: number = 0;

  playlistSchedule: any = {
    IsEveryday: true,
    IsForFullDay: true,
    Days: null,
    StartTime: null,
    EndTime: null
  };
  playerForm: FormGroup;
  scheduleFormGroupKey = 'Schedule';
  hasReceivedScheduleFormGroup = false;
  isMasterDataLoaded = false;
  isControlCenterDataReceived = false;
  showRemoveDeviceIcon = false;
  isBlurApiCall = false;
  invalidRegistrationCodeMsg = null;
  ngUnsubscribe: Subject<any> = new Subject<any>();

  get IsNewDeviceSelected() { return this.playerForm.get('IsNewDeviceSelected'); }
  get DeviceListCheckBoxGroup() { return this.playerForm.get('DeviceListCheckBoxGroup') as FormArray; }
  get PlaylistListCheckBoxGroup() { return this.playerForm.get('PlaylistListCheckBoxGroup') as FormArray; }
  get Orientation() { return this.playerForm.get('Orientation') }
  get IsPortrait() { return this.playerForm.get('Orientation.IsPortrait'); }
  get IsLandscape() { return this.playerForm.get('Orientation.IsLandscape'); }
  get DisplaySizeListCheckBoxGroup() { return this.playerForm.get('DisplaySizeListCheckBoxGroup') as FormArray; }
  get Schedule() { return this.playerForm.get(this.scheduleFormGroupKey) }
  get ScheduleIsEveryday() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'IsEveryday'); }
  get ScheduleIsForFullDay() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'IsForFullDay'); }
  get ScheduleTheseDays() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'TheseDays'); }
  get ScheduleThisTime() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'ThisTime'); }
  get ScheduleTheseDaysCheckBoxGroup() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'TheseDaysCheckBoxGroup') as FormArray; }
  get ScheduleStartTime() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'StartTime'); }
  get ScheduleEndTime() { return this.playerForm.get(this.scheduleFormGroupKey + '.' + 'EndTime'); }

  get selectedDevice(): any {
    let device = this.DeviceListCheckBoxGroup.controls.filter(control => control.value.Checked)
    return device.length > 0 ? device[0].value: undefined;
  }

  get selectedPlaylist(): any {
    return this.PlaylistListCheckBoxGroup.controls.filter(control => control.value.Checked)[0].value;
  }

  get selectedDisplaySize() {
    let displaySize = this.DisplaySizeListCheckBoxGroup.controls.filter(control => control.value.Checked)
    return displaySize.length > 0 ? displaySize[0].value: undefined;
  }

  get selectedDays() {
    let selectedDays = this.ScheduleTheseDaysCheckBoxGroup.controls
      .filter(control => control.value.Checked)
      .map(days =>
        days.value.Id
      ).join(',');

    return this.commonFunctionsService.formatDaysFromNumbers(selectedDays);
  }

  get isNewDeviceFormValid() {
    const firstItem = this.DeviceListCheckBoxGroup.controls
      .filter((value, index) => index == 0)
      .map(control => control.value)[0];

    return firstItem.Checked
      && (firstItem.NewDevice.Code != null && firstItem.NewDevice.Code !== '')
      && (firstItem.NewDevice.Name != null && firstItem.NewDevice.Name !== '')
      ? true
      : false;
  }

  get isNewDeviceCodeValid() {
    const firstItem = this.DeviceListCheckBoxGroup.controls
      .filter((value, index) => index == 0)
      .map(control => control.value)[0];

    return firstItem.Checked
      && (firstItem.NewDevice.Code != null && firstItem.NewDevice.Code !== '')
      ? true
      : false;
  }

  get isNewDeviceNameValid() {
    const firstItem = this.DeviceListCheckBoxGroup.controls
      .filter((value, index) => index == 0)
      .map(control => control.value)[0];

    return firstItem.Checked
      && (firstItem.NewDevice.Name != null && firstItem.NewDevice.Name !== '')
      ? true
      : false;
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private controlCenterService: ControlCenterService,
    private playlistService: PlaylistService,
    private deviceService: DevicesService,
    private commonFunctionsService: CommonFunctionsService,
    private notifyEventService: NotifyEventService) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.controlCenter
      && changes.controlCenter.currentValue !== undefined
      && changes.controlCenter.currentValue != null) {
      this.isControlCenterDataReceived = true;
    }
  }

  ngOnInit() {
    this.buildForm();

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.controlCenterId = params['id'];
        if (this.isMasterDataLoaded) {
          this.getControlCenterDetails();
        }
      }
    });

    this.notifyEventService.deviceConnectionStatusChangeEvent$
    .subscribe((res: any)=>{
      let controlCenters = this.deviceList.filter(x=>x.UniqueId === res.deviceUniqueId);
        if(controlCenters.length > 0){
          controlCenters[0].IsOnline = res.IsOnline;
          if(res.IsOnline){
            controlCenters[0].OfflineDatetime = null;
          }
          else{
            let OfflineDatetime = new Date();
            let milliseconds = OfflineDatetime.getMilliseconds();
            OfflineDatetime.setMilliseconds((OfflineDatetime.getTimezoneOffset() *60 * 1000) + milliseconds);
            controlCenters[0].OfflineDatetime = OfflineDatetime;
          }

          this.addSelectedDeviceCheckBoxGroupControl();

          this.DeviceListCheckBoxGroup.controls.forEach((formGroup: FormGroup) => {
            if (formGroup.controls['Id'].value === this.controlCenter.Id)
              formGroup.controls['Checked'].setValue(true);
            else
              formGroup.controls['Checked'].setValue(false);
          });
          
          this.DeviceListCheckBoxGroup.updateValueAndValidity();
        }
    });

    this.getInitialData();
  }

  formInitialized(name: string, form: FormGroup) {
    this.playerForm.setControl(name, form);
    this.hasReceivedScheduleFormGroup = true;
    this.Schedule.updateValueAndValidity();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngAfterViewInit() {
    //this.showPlayerSetupGuide();
  }

  showPlayerSetupGuide() {
    this.isGuideOpened = true;
  }

  hidePlayerSetupGuide() {
    this.isGuideOpened = false;
  }

  buildForm() {
    let dayNames = this.commonFunctionsService.playlistScheuleDays.map((value, index) => {
      return { Id: index + 1, Text: value, Checked: false };
    });
    let theseDaysCheckBoxGroup = new FormArray(dayNames.map(item => this.formBuilder.group({
      Id: [item.Id],
      Text: [item.Text],
      Checked: [item.Checked]
    })));

    this.playerForm = this.formBuilder.group({
      'IsNewDeviceSelected': [false],
      'DeviceListCheckBoxGroup': this.formBuilder.array([this.createDeviceFormGroup()], this.deviceSelectedValidator(1)),
      'PlaylistListCheckBoxGroup': this.formBuilder.array([], this.minSelectedCheckboxes(1)),
      'Orientation': this.formBuilder.group({
        'IsLandscape': [true],
        'IsPortrait': [null]
      }, null),
      'DisplaySizeListCheckBoxGroup': this.formBuilder.array([], null),
      'Schedule': this.formBuilder.group({
        'IsEveryday': [true],
        'TheseDays': [false],
        'IsForFullDay': [true],
        'ThisTime': [false],
        'TheseDaysCheckBoxGroup': theseDaysCheckBoxGroup,
        'StartTime': [null],
        'EndTime': [null]
      })
    });

    // this.IsNewDeviceSelected.valueChanges.subscribe((isNewDeviceSelected) => {
    //   //get first control in device list
    //   let newDeviceFormGroup = this.DeviceListCheckBoxGroup.controls.filter((value, index) => index === 0)[0];
    //   if (isNewDeviceSelected) {
    //     //new device is selected
    //     newDeviceFormGroup.get('NewDevice.Code').setValidators(Validators.required);
    //     newDeviceFormGroup.get('NewDevice.Name').setValidators(Validators.required);
    //   }
    //   else {
    //     newDeviceFormGroup.get('NewDevice.Code').clearValidators();
    //     newDeviceFormGroup.get('NewDevice.Name').clearValidators();
    //   }

    //   newDeviceFormGroup.updateValueAndValidity();
    //   this.DeviceListCheckBoxGroup.updateValueAndValidity();
    // });
  }

  createDeviceFormGroup(device: IDeviceList = undefined) {
    if(device !== undefined && device !== null && device.Id > 0) {
      return this.formBuilder.group({
        'Id': [device.Id],
        'Text': [device.Name],
        'Checked': [device.Checked],
        'IsOnline': [device.IsOnline],
        'Playlist': [device.Playlist],
        'NewDevice': this.formBuilder.group({
          'Code': [null],
          'Name': [null]
        }),
      });
    }

    //create default device form group
    return this.formBuilder.group({
      'Id': [this.defaultDeviceCheckBox.Id],
      'Text': [this.defaultDeviceCheckBox.Name],
      'Checked': [this.defaultDeviceCheckBox.Checked],
      'IsOnline': [null],
      'Playlist': [null],
      'NewDevice': this.formBuilder.group({
        'Code': [null],
        'Name': [null]
      }),
    });
  }

  createPlaylistFormGroup(playlist: IPlaylistList){
    if(playlist !== undefined && playlist !== null && playlist.Id > 0) {
      return this.formBuilder.group({
        'Id': [playlist.Id],
        'Text': [playlist.Name],
        'Checked': [playlist.Checked]
      });
    }

    return null;
  }

  eitherOrFieldRequired(fg: FormGroup) {
    const isPortrait = fg.get('IsPortrait').value;
    const isLandscape = fg.get('IsLandscape').value;

    if ((isPortrait != null && isPortrait) || (isLandscape != null && isLandscape))
      return null;
    else
      return { required: true };
  }

  deviceSelectedValidator(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = this.getSelectedCheckBoxCount(formArray, 0);
      const firstItem = formArray.controls.filter((value, index) => index == 0).map(control => control.value)[0];

      // return !firstItem.Checked
      //   ? (totalSelected >= min ? null : { required: true })
      //   : ((firstItem.NewDevice.Code != null && firstItem.NewDevice.Name != null)
      //     ? null
      //     : { required: true }
      //   );

      return !firstItem.Checked
        ? (totalSelected >= min ? null : { required: true })
        : { required: true };
    };

    return validator;
  }

  minSelectedCheckboxes(min: number = 1, ignoreIndex: number = null) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = this.getSelectedCheckBoxCount(formArray, ignoreIndex);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  getSelectedCheckBoxCount(formArray: FormArray, ignoreIndex: number = null) {
    const totalSelected = formArray.controls
      .filter((value, index) => ignoreIndex == null || index != ignoreIndex)
      // get a list of checkbox values (boolean)
      .map(control => control.value.Checked)
      // total up the number of checked checkboxes
      .reduce((prev, next) => next ? prev + next : prev, 0);

    return totalSelected;
  }

  deSelectDevice(id){
    let selectedDevice = this.DeviceListCheckBoxGroup.controls.filter(control => control.value.Id === id)[0] as FormGroup;

    selectedDevice.controls['Checked'].setValue(false);
    this.DeviceListCheckBoxGroup.updateValueAndValidity();
    this.showRemoveDeviceIcon = false;
  }

  addSelectedDeviceCheckBoxGroupControl() {
    if (this.controlCenter !== undefined
      && this.controlCenter != null
      && this.controlCenter.Id > 0) {
      let deviceLength = this.deviceList.filter(x => x.Id == this.controlCenter.Id).length;

      if (deviceLength == 0) {
        this.deviceList.splice(1, 0, ...this.controlCenter)
      }

      //remove all device checkbox controls
      this.DeviceListCheckBoxGroup.controls = [];
      //Add default checkbox first for adding a new device
      this.DeviceListCheckBoxGroup.controls.push(this.createDeviceFormGroup());

      let deviceListCheckBoxGroup = this.deviceList.map(item => this.createDeviceFormGroup(item));

      //append device checkbox controls
      this.DeviceListCheckBoxGroup.controls = this.DeviceListCheckBoxGroup.controls.concat(deviceListCheckBoxGroup);
    }

  }

  addSelectedPlaylistCheckBoxGroupControl() {
    if (this.controlCenter.Playlist != undefined
      && this.controlCenter.Playlist != null
      && this.controlCenter.Playlist.Id > 0) {
      let playlistLength = this.playlistList.filter(x => x.Id == this.controlCenter.Playlist.Id).length;

      if (playlistLength == 0) {
        this.playlistList.splice(1, 0, ...this.controlCenter.Playlist)

        //remove and push playlist from this list
        this.PlaylistListCheckBoxGroup.controls = [];

        let PlaylistListCheckBoxGroup = this.playlistList.map(item => this.createPlaylistFormGroup(item));

        //append playlist checkbox controls
        this.PlaylistListCheckBoxGroup.controls = this.PlaylistListCheckBoxGroup.controls.concat(PlaylistListCheckBoxGroup);
      }
    }
  }

  getInitialData() {
    this.playerSetupMenus = [];
    this.playerSetupMenus.push({ Text: 'Summary', Selected: true });
    this.playerSetupMenus.push({ Text: 'Device', Selected: false });
    this.playerSetupMenus.push({ Text: 'Playlist', Selected: false });

    //this.deviceService.generateRegistrationCode()

    forkJoin([this.playlistService.getDisplaySizeList(),
    this.playlistService.getPlaylists(this.PlaylistPageStart, this.PlaylistPageSize),
    this.deviceService.getAvailableDeviceList(this.DevicePageStart, this.DevicePageSize),])
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((responses) => {

        this.displaySizeList = responses[0];
        this.onPlaylistReceiveData(responses[1]);
        this.onDeviceReceiveData(responses[2], false);
        //to-do: need to remove auto generate registration code after app update
        //this.onDeviceRegistrationCodeReceive(responses[3]);

        this.bindInitialFormData();
        this.isMasterDataLoaded = true;
        if (this.isControlCenterDataReceived)
          this.bindControlCenterDetails();
        else
          this.getControlCenterDetails();

        this.isLoading = false;
        this.isDeviceLoading = false;
      }, (error: any) => {
        this.isLoading = false;
        this.isDeviceLoading = false;
        this.commonFunctionsService.displayErrorMessage();
        this.backToControlCenter();
      });
  }

  resetDeviceList() {
    //mark all device checkeboxes as un-checked
    this.DeviceListCheckBoxGroup.controls.forEach((element: FormGroup) => {
      element.controls['Checked'].setValue(false);
    });

    //mark new device selected section as hidden
    this.IsNewDeviceSelected.setValue(false);
    //remove all the existing devices
    this.deviceList = [];
    //reset pagination to the start of the page
    this.DevicePageStart = 0;
    //reset loaded total records count to 0
    this.DeviceTotalRecords = 0;

    this.isDeviceLoading = true;
    //this.deviceService.generateRegistrationCode()
    forkJoin([this.deviceService.getAvailableDeviceList(this.DevicePageStart, this.DevicePageSize),
    ])
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((responses) => {
        this.onDeviceReceiveData(responses[0], true);
        //to-do: need to remove auto generate registration code after app update
        //this.onDeviceRegistrationCodeReceive(responses[1]);
        this.isDeviceLoading = false;
      }, (error: any) => {
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  getControlCenterDetails() {
    if (this.controlCenterId > 0) {
      this.controlCenterService.getControlCenterDetails(this.controlCenterId)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res: any) => {
          if (res.Id > 0) {
            this.controlCenter = res;

            this.bindControlCenterDetails();
          }
          else {
            this.backToControlCenter();
          }
        }, (error: any) => {
          this.commonFunctionsService.displayErrorMessage();
          this.backToControlCenter();
        });
    }
  }

  backToControlCenter() {
    this.router.navigate(['/control-center']);
  }

  bindInitialFormData() {

    let deviceListCheckBoxGroup = this.deviceList.map(item => this.createDeviceFormGroup(item));

    let playlistListCheckBoxGroup = this.playlistList.map(item => this.createPlaylistFormGroup(item));

    let displaySizeCheckBoxGroup = this.displaySizeList.map((item, index) => this.formBuilder.group({
      Id: [item.Id],
      Text: [item.Name],
      Checked: [false]
    }));

    this.DeviceListCheckBoxGroup.controls = this.DeviceListCheckBoxGroup.controls.concat(deviceListCheckBoxGroup);
    this.PlaylistListCheckBoxGroup.controls = this.PlaylistListCheckBoxGroup.controls.concat(playlistListCheckBoxGroup);
    this.DisplaySizeListCheckBoxGroup.controls = this.DisplaySizeListCheckBoxGroup.controls.concat(displaySizeCheckBoxGroup);

    //this.DeviceListCheckBoxGroup.updateValueAndValidity();
    //this.PlaylistListCheckBoxGroup.updateValueAndValidity();
    this.DisplaySizeListCheckBoxGroup.updateValueAndValidity();
  }

  bindControlCenterDetails() {
    this.addSelectedDeviceCheckBoxGroupControl();
    this.addSelectedPlaylistCheckBoxGroupControl();

    this.DeviceListCheckBoxGroup.controls.forEach((formGroup: FormGroup) => {
      if (formGroup.controls['Id'].value === this.controlCenter.Id)
        formGroup.controls['Checked'].setValue(true);
      else
        formGroup.controls['Checked'].setValue(false);
    });

    this.PlaylistListCheckBoxGroup.controls.forEach((formGroup: FormGroup) => {
      if (formGroup.controls['Id'].value === this.controlCenter.Playlist.Id)
        formGroup.controls['Checked'].setValue(true);
      else
        formGroup.controls['Checked'].setValue(false);
    });

    if (this.controlCenter.IsPortrait != null) {
      this.IsPortrait.setValue(this.controlCenter.IsPortrait);
      this.IsLandscape.setValue(!this.controlCenter.IsPortrait);
    }
    else {
      this.IsPortrait.setValue(false);
      this.IsLandscape.setValue(true);
    }

    if (this.controlCenter.DisplaySize != null && this.controlCenter.DisplaySize.Id > 0) {
      this.DisplaySizeListCheckBoxGroup.controls.forEach((formGroup: FormGroup) => {
        if (formGroup.controls['Id'].value === this.controlCenter.DisplaySize.Id)
          formGroup.controls['Checked'].setValue(true);
        else
          formGroup.controls['Checked'].setValue(false);
      });
    }
    // else {
    //   this.DisplaySizeListCheckBoxGroup.controls.forEach((formGroup: FormGroup, index: number) => {
    //     if (index === 0)
    //       formGroup.controls['Checked'].setValue(true);
    //     else
    //       formGroup.controls['Checked'].setValue(false);
    //   });
    // }

    this.DeviceListCheckBoxGroup.updateValueAndValidity();
    this.PlaylistListCheckBoxGroup.updateValueAndValidity();
    this.DisplaySizeListCheckBoxGroup.updateValueAndValidity();

    //update playlistschdule object from selected device/playlist
    this.playlistSchedule = {
      IsEveryday: this.controlCenter.Playlist.Id > 0 ? this.controlCenter.Playlist.IsEveryday : true,
      IsForFullDay: this.controlCenter.Playlist.Id > 0 ? this.controlCenter.Playlist.IsForFullDay : true,
      Days: this.controlCenter.Playlist.Days,
      StartTimeMinutes: this.controlCenter.Playlist.StartTimeMinutes,
      EndTimeMinutes: this.controlCenter.Playlist.EndTimeMinutes
    };

    this.bindPlaylistScheduleDetails();

    this.hasReceivedScheduleFormGroup = false;
  }

  bindPlaylistScheduleDetails(){
    this.ScheduleIsEveryday.setValue(this.playlistSchedule.IsEveryday);
    this.ScheduleIsForFullDay.setValue(this.playlistSchedule.IsForFullDay);
    
    if(this.playlistSchedule.Days !== undefined && this.playlistSchedule.Days !=null) {
      let dayNumbers = this.playlistSchedule.Days.split(',');
      this.ScheduleTheseDaysCheckBoxGroup.controls.forEach((control: FormGroup) => {
        if (dayNumbers.includes(String(control.controls['Id'].value)))
          control.controls['Checked'].setValue(true);
        else
          control.controls['Checked'].setValue(false);
      });
    }
    
    this.ScheduleIsForFullDay.setValue(this.playlistSchedule.IsForFullDay);
    this.ScheduleStartTime.setValue(this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.StartTimeMinutes));
    this.ScheduleEndTime.setValue(this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.EndTimeMinutes));
  }

  getPlaylists() {
    this.playlistService.getPlaylists(this.PlaylistPageStart, this.PlaylistPageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => this.onPlaylistReceiveData(res));
  }

  onPlaylistReceiveData(res: any) {
    if (res !== undefined && res != null) {
      this.PlaylistTotalRecords = res.TotalCount;

      if (this.controlCenterId > 0) {
        if (this.PlaylistPageStart > 0) {
          let index = res.PlayLists.findIndex(x => x.Id == this.controlCenter.Playlist.Id);
          if (index > -1)
            this.playlistList.splice(index, 1);
        }
      }

      if (this.PlaylistPageStart > 0)
        this.PlaylistListCheckBoxGroup.controls.splice(0, this.PlaylistListCheckBoxGroup.controls.length);

      let playlistListCheckBoxGroup = this.playlistList.map(item => this.createPlaylistFormGroup(item));

      this.PlaylistListCheckBoxGroup.controls = this.PlaylistListCheckBoxGroup.controls.concat(playlistListCheckBoxGroup);


      this.playlistList = this.playlistList.concat(res.PlayLists);

      this.PlaylistIsLoadMore = this.playlistList.length < this.PlaylistTotalRecords;
    }
  }

  getDevices() {
    this.deviceService.getAvailableDeviceList(this.DevicePageStart, this.DevicePageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        //before removing any controls get copy of registrationCode i.e. PIN Code and Name
        let firstItem = this.DeviceListCheckBoxGroup.controls
          .filter((value, index) => index == 0)
          .map((control: any) => control.controls.NewDevice.value)[0];

        let registrationCode = firstItem.Code;
        let name = firstItem.Name;

        this.onDeviceReceiveData(res);

        //get latest object of the same
        firstItem = this.DeviceListCheckBoxGroup.controls
          .filter((value, index) => index == 0)
          .map((control: any) => control.controls.NewDevice.controls)[0];

        //update the name and code
        firstItem.Code.setValue(registrationCode);
        firstItem.Name.setValue(name);
      });
  }

  onDeviceReceiveData(res: any, isReset: boolean = false) {
    if (res !== undefined && res != null) {
      this.DeviceTotalRecords = res.TotalCount;

      if (this.controlCenterId > 0) {
        if (this.DevicePageStart > 0 && !isReset) {
          let index = res.Devices.findIndex(x => x.Id == this.controlCenter.Id);
          if (index > -1)
            this.deviceList.splice(index, 1);
        }
      }
      this.deviceList = this.deviceList.concat(res.Devices);

      if (this.DevicePageStart > 0 || isReset) {
        //remove all device checkbox controls
        this.DeviceListCheckBoxGroup.controls = [];
        //Add default checkbox first for adding a new device
        this.DeviceListCheckBoxGroup.controls.push(this.createDeviceFormGroup());

        let deviceListCheckBoxGroup = this.deviceList.map(item => this.createDeviceFormGroup(item));

        //append device checkbox controls
        this.DeviceListCheckBoxGroup.controls = this.DeviceListCheckBoxGroup.controls.concat(deviceListCheckBoxGroup);

        if(isReset){
          let newDevice = this.deviceList.filter(x => x.Id === this.controlCenterId);
          if(newDevice.length > 0){
            this.controlCenter = newDevice[0];
            this.controlCenterId = 0;
            //update all other steps (playlist, orientation, size, schedule)
            this.bindControlCenterDetails();

            //to fire the validation of playlist form array
            this.DeviceListCheckBoxGroup.updateValueAndValidity();
          }
        }
      }

      this.DeviceIsLoadMore = this.deviceList.length < this.DeviceTotalRecords;
    }
  } 

  onDeviceRegistrationCodeReceive(res: any) {
    if (res !== undefined && res != null) {
      let firstItem = this.DeviceListCheckBoxGroup.controls
        .filter((value, index) => index == 0)
        .map((control: any) => control.controls.NewDevice.controls.Code)[0];

      firstItem.setValue(res.RegistrationCode);
      firstItem.updateValueAndValidity();
    }
  }

  onScrollDownPlaylistList(event) {
    if (this.PlaylistIsLoadMore) {
      this.PlaylistPageStart++;
      this.getPlaylists();
    }
  }

  onScrollDownDeviceList(event) {
    if (this.DeviceIsLoadMore) {
      this.DevicePageStart++;
      this.getDevices();
    }
  }

  onChangeDevice(event, selectedIndex) {
    if (event.target.checked) {
      let filteredDeviceList = this.DeviceListCheckBoxGroup.controls.filter((value, index) => index != selectedIndex);
      filteredDeviceList.forEach((element: FormGroup) => {
        element.controls['Checked'].setValue(false);
      });

      if (selectedIndex !== 0) {
        //get selected device value
        this.controlCenter = this.deviceList.filter(x => x.Id === this.selectedDevice.Id)[0];

        //update all other steps (playlist, orientation, size, schedule)
        this.bindControlCenterDetails();
      }
    }

    this.IsNewDeviceSelected.setValue(event.target.checked && selectedIndex === 0);
    //to fire the validation of device form array
    this.DeviceListCheckBoxGroup.updateValueAndValidity();
  }

  onChangePlaylist(event, selectedIndex) {
    if (event.target.checked) {
      let filteredPlaylistList = this.PlaylistListCheckBoxGroup.controls.filter((value, index) => index != selectedIndex);
      filteredPlaylistList.forEach((element: FormGroup) => {
        element.controls['Checked'].setValue(false);
      });

      let playlist = this.playlistList.filter(x => x.Id === this.selectedPlaylist.Id)[0];

      //get the latest schedule from the playlist
      this.playlistSchedule = {
        IsEveryday: playlist.IsEveryday,
        IsForFullDay: playlist.IsForFullDay,
        Days: playlist.Days,
        StartTimeMinutes: playlist.StartTimeMinutes,
        EndTimeMinutes: playlist.EndTimeMinutes
      };

      this.bindPlaylistScheduleDetails();

      this.hasReceivedScheduleFormGroup = false;
    }

    //to fire the validation of playlist form array
    this.PlaylistListCheckBoxGroup.updateValueAndValidity();
  }

  onChangePortrait(event) {
    this.IsLandscape.setValue(!event.target.checked);
  }

  onChangeLandscape(event) {
    this.IsPortrait.setValue(!event.target.checked);
  }

  onChangeDisplaySize(event, selectedIndex) {
    if (event.target.checked) {
      let filteredDisplaySizeList = this.DisplaySizeListCheckBoxGroup.controls.filter((value, index) => index !== selectedIndex);
      filteredDisplaySizeList.forEach((element: FormGroup) => {
        element.controls['Checked'].setValue(false);
      });
    }
    else {
      let filteredDisplaySizeList = this.DisplaySizeListCheckBoxGroup.controls.filter((value, index) => index === selectedIndex);
      filteredDisplaySizeList.forEach((element: FormGroup) => {
        element.controls['Checked'].setValue(!event.target.checked);
      });
    }
  }

  selectPlayerSetupMenu(playerSetupMenu: any, index: number) {
    this.playerSetupMenus.forEach(element => {
      element.Selected = false;
    });

    if (index === 5) {
      let selectedDays = this.ScheduleTheseDaysCheckBoxGroup.controls
        .filter(control => control.value.Checked)
        .map(days =>
          days.value.Id
        ).join(',');

      this.playlistSchedule = {
        IsEveryday: this.ScheduleIsEveryday.value,
        IsForFullDay: this.ScheduleIsForFullDay.value,
        Days: selectedDays,
        StartTime: this.ScheduleStartTime.value,
        EndTime: this.ScheduleEndTime.value
      };
      this.hasReceivedScheduleFormGroup = false;
    }

    playerSetupMenu.Selected = true;
  }

  hasFormSectionValid(index) {
    let isValid = false;
    switch (index) {
      case 1:
        isValid = this.DeviceListCheckBoxGroup.valid;
        break;
      case 2:
        isValid = this.PlaylistListCheckBoxGroup.valid;
        break;
      case 3:
        isValid = this.Orientation.valid;
        break;
      case 4:
        isValid = this.DisplaySizeListCheckBoxGroup.valid;
        break;
      case 5:
        isValid = this.Schedule.valid;
        break;
      default:
        break;
    }

    return isValid;
  }

  isControlCenterActive(){
    if(this.playerForm.valid && this.selectedDevice !== undefined){
      let device = this.deviceList.filter(x => x.Id === this.selectedDevice.Id)[0];
      return device.Playlist !== undefined && device.Playlist !=null && device.Playlist.Id > 0;
    }
    
    return false;
  }

  getDeviceDetailsByCode() {
    if (this.selectedDevice.NewDevice.Code !== undefined && this.selectedDevice.NewDevice.Code !== null) {
      this.invalidRegistrationCodeMsg = null;
      this.isBlurApiCall = true;

      let firstDevice = this.DeviceListCheckBoxGroup.controls
        .filter((value, index) => index == 0)
        .map((control: any) => control.controls.NewDevice.controls)[0];

      this.deviceService.getDeviceDetailsByCode(this.selectedDevice.NewDevice.Code)
        .subscribe((res: any) => {
          if(res !== undefined && res != null && res.Id > 0){
            firstDevice.Name.setValue(res.Name);
          }

          this.isBlurApiCall = false;
          firstDevice.Code.updateValueAndValidity();
        }, (error: any) => {
          this.isBlurApiCall = false;

          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null){
            if(error.error.error_type === 'Validation'){
              this.invalidRegistrationCodeMsg = error.error.data;
              firstDevice.Code.updateValueAndValidity();
            }
            else{
              this.commonFunctionsService.displayErrorMessage(error.error.error_description);
            }
          }
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }
  }

  saveNewDevice() {
    if (this.isNewDeviceFormValid) {
      let saveDeviceModel: any = {
        Name: this.selectedDevice.NewDevice.Name,
        RegistrationCode: this.selectedDevice.NewDevice.Code
      };

      //to-do: need to update method after app verification to verify existing device only
      this.deviceService.verifyDevice(saveDeviceModel)
        .subscribe(res => {
          if(res.IsVerified){
            this.commonFunctionsService.displaySuccessMessage('Device successfully added');
            this.controlCenterId = res.Id;
            this.resetDeviceList();
          }
        }, (error: any) => {
          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null){
            if(error.error.error_type === 'Validation'){
              this.commonFunctionsService.displayErrorMessage(error.error.data);
            }
            else{
              this.commonFunctionsService.displayErrorMessage(error.error.error_description);
            }
          }
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }
  }

  savePlayerSetup() {
    if (this.playerForm.valid) {
      let controlCenterSaveModel: any = {};
      if (!this.IsNewDeviceSelected.value) {
        controlCenterSaveModel.DeviceId = this.selectedDevice.Id;
      }
      controlCenterSaveModel.PlaylistId = this.selectedPlaylist.Id;
      
      this.controlCenterService.saveControlCenterDetails(controlCenterSaveModel)
        .subscribe((res: any) => {
          this.commonFunctionsService.displaySuccessMessage('Player Setup saved successfully!');
          //this.router.navigate(['control-center', 'player-setup', res.Id]);
          this.notifyEventService.onPlayerSetup(true);
        }, (error: any) => {
          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null)
            this.commonFunctionsService.displayErrorMessage(error.error.error_description);
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }
  }

  cancelForm() {
    this.notifyEventService.onPlayerSetup(false);
  }

  removeControlCenter() {
    if (this.selectedDevice != null && this.selectedDevice.Id > 0) {
      this.controlCenterService.removeControlCenter(this.selectedDevice.Id)
        .subscribe((res: any) => {
          this.commonFunctionsService.displaySuccessMessage('Player Setup details removed successfully!');
          this.notifyEventService.onPlayerSetup(true);
        }, (error: any) => {
          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null)
            this.commonFunctionsService.displayErrorMessage(error.error.error_description);
          else
            this.commonFunctionsService.displayErrorMessage();
        }
        );
    }

  }

  goToCreatePlaylist(){
    this.router.navigate(['/playlists', 'manage-playlist']);
    this.notifyEventService.onPlayerSetup(false);
  }
  
  rebootDevice(){
    this.deviceService.rebootDevice(this.selectedDevice.UniqueId).subscribe(res=>{
      this.commonFunctionsService.displaySuccessMessage('Device reboot command sent successfully!');
    }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }
}
