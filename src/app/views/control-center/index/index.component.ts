import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { NotifyEventService } from 'src/app/services/notify-event.service';
import { ControlCenterService } from 'src/app/services/control-center.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ModalService } from 'src/app/services/modal.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { PlayerSetupModalComponent } from '../../modal-templates/player-setup-modal/player-setup-modal.component';
import { SortableSpec, DraggedItem } from '@angular-skyhook/sortable';
import { IDeviceList } from 'src/app/interfaces/IDeviceList';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  currentUrl: string = '';
  ngUnsubscribe: Subject<any> = new Subject<any>();

  scrollDistance = 1;
  throttle = 300;
  isLoading = false;
  pageSize = 10;
  TotalRecords = 0;
  PageStart = 0;
  controlCenterList: IDeviceList[] = [];
  controlCenterListTemp: IDeviceList[] = [];
  isLoadMore = true;
  
  constructor(public router: Router,
              private route: ActivatedRoute,
              private modalService: ModalService,
              private notifyEventService: NotifyEventService,
              private commonFunctionsService: CommonFunctionsService,
              private controlCenterService: ControlCenterService) { 
    this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd) {
        this.currentUrl = event.url;
      }
    })
  }

  ngOnInit() {
    this.notifyEventService.playerSetupEvent$.subscribe(res=>{
      //refresh the view
      if(res){
        console.log('player setup saved, can refresh the view');

        this.PageStart = 0;
        this.TotalRecords = 0;
        this.controlCenterList = [];
        this.getControlCenterList();
      }
      else
        console.log('player setup cancelled');
    })

    this.notifyEventService.deviceConnectionStatusChangeEvent$.subscribe((res: any)=>{
      //refresh the view
      if(res.deviceUniqueId){
        console.log(res.deviceUniqueId);

        let controlCenters = this.controlCenterList.filter(x=>x.UniqueId === res.deviceUniqueId);
        if(controlCenters.length > 0){
          controlCenters[0].IsOnline = res.IsOnline;
          if(res.IsOnline){
            controlCenters[0].OfflineDatetime = null;
          }
          else{
            let OfflineDatetime = new Date();
            let milliseconds = OfflineDatetime.getMilliseconds();
            OfflineDatetime.setMilliseconds((OfflineDatetime.getTimezoneOffset() *60 * 1000) + milliseconds);
            controlCenters[0].OfflineDatetime = OfflineDatetime;
          }
        }
        
        this.controlCenterDeviceSpec.endDrag(null);
      }
      else
        console.log('device status change: not found');
    })

    this.getControlCenterList();

    //this.goToPlayerSetup();
  }

  move(item: DraggedItem<any>) {
    // shallow clone the list
    // do this so we can avoid overwriting our 'saved' list.
    const temp = this.controlCenterList.slice(0);
    // delete where it was previously
    temp.splice(item.index, 1);
    // add it back in at the new location
    temp.splice(item.hover.index, 0, item.data);
    return temp;
  }
  
  controlCenterDeviceSpec: SortableSpec<any> = {
    type: "PRIORITY",
    // trackBy is required
    trackBy: x => x.Id,
    hover: item => {
      this.controlCenterListTemp = this.move(item);
    },
    drop: item => { // save the changes
      this.controlCenterListTemp = this.controlCenterList = this.move(item);
      console.log('item dropped')
      this.updateDisplayOrder();
    },
    endDrag: _item => { // revert
      this.controlCenterListTemp = this.controlCenterList;
    }
  }

  getControlCenterList() {
    this.isLoading = true;
    this.controlCenterService.getControlCenterList(this.PageStart, this.pageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.TotalRecords = res.TotalCount;

        // res.ControlCenterDeviceList.forEach((element, index) => {
        //   element.IsOnline = index % 2 == 0;
        // });

        this.controlCenterList = this.controlCenterList.concat(res.ControlCenterDeviceList);
        this.controlCenterListTemp = this.controlCenterList;
        this.controlCenterDeviceSpec.endDrag(null);
        this.isLoadMore = this.controlCenterList.length < this.TotalRecords;
        this.isLoading = false;
      }, (error: any) => {
        this.isLoading = false;
        this.PageStart--;
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  onScrollDownControlCenterList(event) {
    // if (this.isLoadMore) {
    //   this.PageStart++;
    //   this.getControlCenterList();
    // }
  }

  updateDisplayOrder(){
    const controlCenterIds: number[] = this.controlCenterList.map(x => x.Id);

    this.controlCenterService.updateDisplayOrder(controlCenterIds).subscribe(res=>{
      console.log('order updated');
    });
  }

  openPlayerSetup(id: number = 0) {
    if(id !== undefined && id > 0){
      this.controlCenterService.getControlCenterDetails(id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        if (res != null && res.Id > 0) {
          const initialState = {
            controlCenter: {...res}
          };

          this.modalService.openModal(PlayerSetupModalComponent, { initialState });
        }

      }, (error: any) => {
        this.commonFunctionsService.displayErrorMessage();
      });
    }
    else{
      const initialState = {
        controlCenter: undefined
      };

      this.modalService.openModal(PlayerSetupModalComponent, { initialState });
    }
  }
}
