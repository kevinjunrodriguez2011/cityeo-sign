import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { DevicesService } from 'src/app/services/devices.service';
import { ApiError } from 'src/app/models/ApiError';
import { IDeviceList } from 'src/app/interfaces/IDeviceList';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NotifyEventService } from 'src/app/services/notify-event.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {
  checkAllDevice: boolean = false;
  checkedDevice: boolean = false;
  browserType: string;
  os: string;
  isLoading: boolean = true;
  PageSizeUnit = 25;
  TotalRecords = 0;
  PageStart = 0;
  PageSize = this.PageSizeUnit;

  devicesList: IDeviceList[] = [];
  TrashdevicesList: IDeviceList[] = [];
  ngUnsubscribe: Subject<any> = new Subject<any>();

  constructor(private deviceService: DevicesService,
    private commonFunctionsService: CommonFunctionsService,
    private notifyEventService: NotifyEventService) {
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os')
  }

  ngOnInit() {
    this.notifyEventService.deviceConnectionStatusChangeEvent$.subscribe((res: any)=>{
      //refresh the view
      if(res.deviceUniqueId){
        console.log(res.deviceUniqueId);

        let controlCenters = this.devicesList.filter(x=>x.UniqueId === res.deviceUniqueId);
        if(controlCenters.length > 0){
          controlCenters[0].IsOnline = res.IsOnline;
          if(res.IsOnline){
            controlCenters[0].OfflineDatetime = null;
          }
          else{
            let OfflineDatetime = new Date();
            let milliseconds = OfflineDatetime.getMilliseconds();
            OfflineDatetime.setMilliseconds((OfflineDatetime.getTimezoneOffset() *60 * 1000) + milliseconds);
            controlCenters[0].OfflineDatetime = OfflineDatetime;
          }
        }
      }
      else
        console.log('player setup cancelled');
    })

    this.isLoading = true;
    this.getDeviceList();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getDeviceList(pageIndex: number = 0) {
    this.isLoading = true;
    this.deviceService.getDeviceList(pageIndex, this.PageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.TotalRecords = res.TotalCount;
        this.PageStart = pageIndex + 1;
        this.devicesList = res.Devices;

        this.devicesList.forEach(element => {
          const found = this.TrashdevicesList.find(
            x => x.Id === element.Id
          );
          if (found !== undefined) {
            element.Checked = true;
          }
          element.ModifiedDatetime = new Date(element.ModifiedDatetime + 'Z');
        });

        const list = this.devicesList.filter(x => x.Checked == true);
        this.checkAllDevice = list.length === this.PageSize ? true : false;
        this.checkedDevice = list.length > 0 ? true : false;
        this.isLoading = false;
      }, (error: any) => {
        this.isLoading = false;
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  setPage(page) {
    this.getDeviceList(page)
  }

  setPageSize(pageSize) {
    this.PageSize = pageSize;
    this.getDeviceList(0);
  }

  checkDevice(event, device) {
    device.Checked = event.target.checked;
    if (event.target.checked) {
      this.checkedDevice = true;
    }
    else {
      this.TrashdevicesList = this.TrashdevicesList.filter(x => x.Id != device.Id);
      const selected = this.devicesList.filter(x => x.Checked);
      this.checkedDevice = selected.length > 0 ? true : false;
    }
    this.AddItemsToTrash();
    //this.checkedDevice = !this.checkedDevice;
    //device.Checked = !this.checkedDevice;
  }

  hasCheckedDevice() {
    return this.devicesList.filter(d => d.Checked).length > 0;
  }
  hasCheckedDeviceAcrossPages() {
    return this.TrashdevicesList.filter(m => m.Checked).length > 0;
  }

  deviceCheckAll(event) {
    this.devicesList.forEach(c => {
      c.Checked = this.checkAllDevice;
    });

    if (!event.target.checked) {
      this.checkedDevice = false;
      this.devicesList.forEach(element => {
        this.TrashdevicesList = this.TrashdevicesList.filter(
          x => x.Id != element.Id
        );
      });
    }
    this.AddItemsToTrash();
  }
  deviceCheckedLength() {
    return this.devicesList.filter(m => m.Checked).length;
  }
  removeDevice(device: IDeviceList) {
    let deviceIdToRemove: number[] = [];
    deviceIdToRemove.push(device.Id as number);

    this.deviceService.removeDevice(deviceIdToRemove)
      .subscribe((res: any) => {
        this.devicesList = this.devicesList.filter(c => c.Id != device.Id);

        const index = this.TrashdevicesList.map(c => c.Id).indexOf(device.Id)

        //remove from the list on individual media remove
        if (index !== undefined) {
          this.TrashdevicesList.splice(index, 1)
        }

        this.commonFunctionsService.displaySuccessMessage('Device removed successfully!');
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }
  removeAllCheckedDevice() {
    this.devicesList = this.devicesList.filter(c => !c.Checked);

    const deviceIdsToRemove: number[] = this.TrashdevicesList.map(x => x.Id);
    this.deviceService.removeDevice(deviceIdsToRemove)
      .subscribe((res: any) => {
        //remove previously selected items
        this.devicesList = this.devicesList.filter(c => !c.Checked);

        //empty thrash array as all items are processed
        this.TrashdevicesList = [];
        this.commonFunctionsService.displaySuccessMessage('Device removed successfully!');
        this.getDeviceList();
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }

  AddItemsToTrash() {
    const selected = this.devicesList.filter(x => x.Checked);
    if (this.TrashdevicesList.length > 0) {
      this.devicesList.forEach(element => {
        const found = this.TrashdevicesList.find(
          x => x.Id === element.Id
        );
        if (found === undefined) {
          if (element.Checked) {
            this.TrashdevicesList.push(element);
          }
        }
      });
    } else {
      this.TrashdevicesList = selected;
    }
  }

  rebootDevice(device: IDeviceList){
    this.deviceService.rebootDevice(device.UniqueId).subscribe(res=>{
      this.commonFunctionsService.displaySuccessMessage('Device reboot command sent successfully!');
    }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }
}
