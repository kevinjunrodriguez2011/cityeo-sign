import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { AddDeviceComponent } from './add-device/add-device.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'add-device',
    component: AddDeviceComponent
  },
  {
    path: 'edit-device/:id',
    component: AddDeviceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevicesRoutingModule { }
