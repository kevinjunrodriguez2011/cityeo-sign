import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DevicesService } from 'src/app/services/devices.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder, AbstractControl, ValidatorFn } from '@angular/forms';
import { ApiError } from 'src/app/models/ApiError';
import { LocationService } from 'src/app/services/location.service';
import { Observable, of, Subject } from 'rxjs';
import { mergeMap, map, takeUntil } from 'rxjs/operators';
import { TypeaheadMatch } from 'ngx-bootstrap';
import { PlaylistService } from 'src/app/services/playlist.service';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit, OnDestroy {
  deviceId: number = 0;
  device: any = {};
  deviceForm: FormGroup;
  isImageUploaded: boolean = false;
  fileUrl: string;
  timezones = [];
  statesDataSource: Observable<any>;
  playlistDataSource: Observable<any>;
  selectedPlaylistId: number;
  isEditMode: boolean = false;
  isBlurApiCall: boolean = false;
  invalidRegistrationCodeMsg = '';
  ngUnsubscribe: Subject<any> = new Subject<any>();

  get Name() { return this.deviceForm.get('Name'); }
  get Address() { return this.deviceForm.get('Address'); }
  get City() { return this.deviceForm.get('City'); }
  get State() { return this.deviceForm.get('State'); }
  get Zip() { return this.deviceForm.get('Zip'); }
  get PhotoUpload() { return this.deviceForm.get('PhotoUpload'); }
  get SerialNumber() { return this.deviceForm.get('SerialNumber'); }
  get RegistrationCode() { return this.deviceForm.get('RegistrationCode'); }
  get RegistrationKey() { return this.deviceForm.get('RegistrationKey'); }
  get DeviceType() { return this.deviceForm.get('DeviceType'); }
  get OsVersion() { return this.deviceForm.get('OsVersion'); }
  get TimezoneId() { return this.deviceForm.get('TimezoneId'); }
  get PlaylistName() { return this.deviceForm.get('PlaylistName'); }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private deviceService: DevicesService,
    private playlistService: PlaylistService,
    private locationService: LocationService,
    private commonFunctionsService: CommonFunctionsService) { }

  ngOnInit() {
    this.buildForm();
    this.getStates();
    this.getTimezones();
    this.getPlaylists();

    this.selectedPlaylistId = null;
    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.deviceId = params['id'];
        this.getDeviceDetails();
      }
      else {
        //to do: remove api call after app setup
        // this.deviceService.generateRegistrationCode().subscribe((res: any) => {
        //   this.RegistrationCode.setValue(res.RegistrationCode);
        // });
      }
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  buildForm() {
    this.deviceForm = this.formBuilder.group({
      'Name': [null, Validators.required],
      'Address': [null],
      'City': [null],
      'State': [null],
      'Zip': [null],
      'PhotoUpload': [null],
      'SerialNumber': [null],
      'RegistrationCode': [null, [Validators.required, Validators.minLength(6), this.validateRegistrationCode()]],
      'RegistrationKey': [null, Validators.required],
      'DeviceType': [null],
      'OsVersion': [null],
      'TimezoneId': [null],
      'PlaylistName': [null],
    });
  }

  validateRegistrationCode(): ValidatorFn {
    const validator: ValidatorFn = (formControl: FormControl) => {
      if (formControl.value !== null && formControl.value !== ""
        && !this.isBlurApiCall && this.invalidRegistrationCodeMsg != '') {
        return { invalidCode: true };
      }
      return null;  
    }
    return validator;
  }

  getStates() {
    this.statesDataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.State.value);
    })
      .pipe(
        mergeMap((token: string) => {
          return this.locationService.getStates(token);
        })
      );
  }

  getPlaylists() {
    this.playlistDataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.PlaylistName.value);
    })
      .pipe(
        mergeMap((token: string) => {
          return this.playlistService.searchPlaylist(token == null ? '' : token);
        }),
        map(({ PlayLists }) =>
          PlayLists
        )
      );
  }

  getTimezones() {
    this.locationService.getTimezones()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.timezones = res;
      })
  }

  getDeviceDetails() {
    if (this.deviceId > 0) {
      this.deviceService.getDeviceDetails(this.deviceId)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res: any) => {
          if (res != null && res.Id > 0) {
            this.device = res;
            this.isEditMode = true;
            this.bindDeviceDetails();
          }
          else {
            this.backToDevices();
          }
        }, (error: any) => {
          this.commonFunctionsService.displayErrorMessage();
          this.backToDevices();
        });
    }
    else {
      this.backToDevices();
    }
  }

  getDeviceDetailsByCode() {
    if (this.RegistrationCode.value !== undefined && this.RegistrationCode.value !== null) {
      this.invalidRegistrationCodeMsg = '';
      this.isBlurApiCall = true;
      this.deviceService.getDeviceDetailsByCode(this.RegistrationCode.value)
        .subscribe((res: any) => {
          if(res !== undefined && res != null && res.Id > 0){
            this.device = res;
            this.deviceId = res.Id;

            this.bindDeviceDetails();
          }
          else{
            // no details found
            this.device = null;
            this.deviceId = 0;
          }
          
          this.isBlurApiCall = false;
          this.RegistrationCode.updateValueAndValidity();
        }, (error: any) => {
          this.isBlurApiCall = false;

          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null){
            if(error.error.error_type === 'Validation'){
              this.invalidRegistrationCodeMsg = error.error.data;
              this.RegistrationCode.updateValueAndValidity();
            }
            else{
              this.commonFunctionsService.displayErrorMessage(error.error.error_description);
            }
          }
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }
  }

  bindDeviceDetails() {
    if(this.device !== undefined && this.device !==null){
      this.deviceForm.patchValue({
        'Name': this.device.Name,
        'Address': this.device.Address,
        'City': this.device.City,
        'State': this.device.State,
        'Zip': this.device.Zip,
        'SerialNumber': this.device.SerialNumber,
        'RegistrationKey': this.device.RegistrationKey,
        'DeviceType': this.device.DeviceType,
        'OsVersion': this.device.OsVersion,
        'TimezoneId': this.device.TimezoneId,
        //'PlaylistName': this.device.Playlist.Name
      });
      
      if(this.isEditMode){
        this.RegistrationCode.setValue(this.device.RegistrationCode);
        this.RegistrationCode.updateValueAndValidity();
      }
      
      this.RegistrationKey.disable();
  
      if (this.device.Photo != null && this.device.Thumbnail != null) {
        this.isImageUploaded = true;
        this.fileUrl = this.device.Thumbnail;
      }
    }
    else{
      this.deviceForm.patchValue({
        'Name': null,
        'Address': null,
        'City': null,
        'State': null,
        'Zip': null,
        'SerialNumber': null,
        'RegistrationCode': null,
        'RegistrationKey': null,
        'DeviceType': null,
        'OsVersion': null,
        'TimezoneId': null,
        //'PlaylistName': null
      });
      this.RegistrationKey.enable();

      this.isImageUploaded = true;
      this.fileUrl = "";
    }
    
  }

  backToDevices() {
    this.router.navigate(['/devices'])
  }

  onFileChanged(files: FileList) {
    const file: File = files[0];
    if (this.commonFunctionsService.validateImageFileType(file.type)) {
      if (this.commonFunctionsService.validateFileSize(file.size)) {
        const reader = new FileReader();

        reader.onload = (event: any) => {
          this.isImageUploaded = true;
          this.fileUrl = event.target.result;
        };

        reader.readAsDataURL(file);
      }
      else {
        this.commonFunctionsService.displayErrorMessage('Invalid file size. Please upload images with file size up to 50MB.');
      }
    }
    else {
      this.commonFunctionsService.displayErrorMessage('Only images are allowed to upload. Uploaded images should contain extensions such as .jpeg, .jpg or .png');
    }
  }

  onSelectState(e: TypeaheadMatch) {
    console.log(e.item.Id)
    console.log(e.item.Name)
    console.log(e.item.value)
  }

  onSelectPlaylist(e: TypeaheadMatch) {
    this.selectedPlaylistId = e.item.Id;
  }

  onBlurPlaylist(event: any) {
    if (this.deviceForm.value.PlaylistName == null || this.deviceForm.value.PlaylistName === '')
      this.selectedPlaylistId = null;
    else
      this.PlaylistName.setValue('');
  }

  onNoResultsPlaylist(hasNoRecords: boolean) {
    if (hasNoRecords) {
      this.PlaylistName.setValue('');
    }

  }

  saveDevice() {
    if (this.deviceForm.valid) {
      let saveDeviceModel: any = {};
      Object.assign(saveDeviceModel, this.deviceForm.value);

      saveDeviceModel.Id = this.deviceId;
      saveDeviceModel.UploadedPhotoBase64 = this.isImageUploaded ? this.fileUrl : '';
      //saveDeviceModel.PlaylistId = this.selectedPlaylistId;

      this.deviceService.saveDevice(saveDeviceModel)
        .subscribe(res => {
          this.commonFunctionsService.displaySuccessMessage('Device saved successfully!');

          this.router.navigate(['devices', 'edit-device', res.Id]);
        }, (error: any) => {
          const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

          if (apiError !== undefined && apiError != null){
            if(apiError.error_type === 'Validation'){
              this.commonFunctionsService.displayErrorMessage(error.error.data);
            }
            else{
              this.commonFunctionsService.displayErrorMessage(error.error.error_description);
            }
            
          }
          else
            this.commonFunctionsService.displayErrorMessage();
        });
    }
  }
}
