import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DevicesRoutingModule } from './devices-routing.module';
import { IndexComponent } from './index/index.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { TypeaheadModule } from 'ngx-bootstrap';
import { PagerModule } from '../pager/pager.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DevicesRoutingModule,
    TypeaheadModule.forRoot(),
    PagerModule
  ],
  declarations: [IndexComponent, AddDeviceComponent],
  exports: [IndexComponent, AddDeviceComponent]
})
export class DevicesModule { }
