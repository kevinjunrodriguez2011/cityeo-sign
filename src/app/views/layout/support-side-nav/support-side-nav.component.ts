import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NotifyEventService } from 'src/app/services/notify-event.service';

@Component({
  selector: 'app-support-side-nav',
  templateUrl: './support-side-nav.component.html',
  styleUrls: ['./support-side-nav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SupportSideNavComponent implements OnInit {
  constructor(private notifyEventService: NotifyEventService) {}

  ngOnInit() {}

  close(){
    this.notifyEventService.onSupportSideBarToggle();
  }
}
