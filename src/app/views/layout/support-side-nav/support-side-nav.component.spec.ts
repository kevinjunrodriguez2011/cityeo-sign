import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportSideNavComponent } from './support-side-nav.component';

describe('SupportSideNavComponent', () => {
  let component: SupportSideNavComponent;
  let fixture: ComponentFixture<SupportSideNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportSideNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportSideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
