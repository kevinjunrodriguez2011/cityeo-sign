import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NotifyEventService } from 'src/app/services/notify-event.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  browserType: string;
  os: string;
  currentUrl: string;
  
  constructor(private router: Router,
    private notifyEventService: NotifyEventService) { 
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os')

    this.router.events.subscribe((event) => {
      if(event instanceof NavigationEnd) {
        this.currentUrl = event.url;
      }
    })
  }

  ngOnInit() {
  }

  toggleRightSidebar(){
    this.notifyEventService.onSupportSideBarToggle();
  }
}
