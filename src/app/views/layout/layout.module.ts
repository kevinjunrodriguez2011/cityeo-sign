import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { SupportSideNavComponent } from './support-side-nav/support-side-nav.component';
import { AccordionModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [HeaderComponent, SideNavComponent, SupportSideNavComponent],
  imports: [CommonModule, RouterModule, AccordionModule.forRoot()],
  exports: [HeaderComponent, SideNavComponent, SupportSideNavComponent]
})
export class LayoutModule {}
