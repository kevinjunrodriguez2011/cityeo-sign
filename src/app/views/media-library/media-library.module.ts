import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MediaLibraryRoutingModule } from './media-library-routing.module';
import { IndexComponent } from './index/index.component';
import { AddMediaComponent } from './add-media/add-media.component';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { PagerModule } from '../pager/pager.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [IndexComponent, AddMediaComponent],
  imports: [
    CommonModule,
    FormsModule,
    MediaLibraryRoutingModule,
    NgxSmartModalModule.forRoot(),
    PagerModule,
    SharedModule
  ],
  exports: [IndexComponent, AddMediaComponent],
  providers: []
})
export class MediaLibraryModule { }
