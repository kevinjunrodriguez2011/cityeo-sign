import { Component, OnInit, ChangeDetectorRef, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MediaLibraryService } from 'src/app/services/media-library.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { ApiError } from 'src/app/models/ApiError';
import { MediaLibrary } from 'src/app/models/MediaLibrary';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  mediaLibraryTypes = [];
  selectedMediaLibraryType = {
    Id: 0,
    Name: 'All Media'
  };
  isLoading: boolean = true;
  mediaLibraries: MediaLibrary[] = [];
  TrashMediaLibraries: MediaLibrary[] = [];
  hidePrevZoomMediaBtn = false;
  hideNextZoomMediaBtn = false;
  zoomedMedia: any = {};
  showAllCheckboxMedia = false;
  checkedMedia: boolean = false;
  checkAllMedia: boolean = false;
  browserType: string;
  os: string;
  PageSizeUnit = 25;
  TotalRecords = 0;
  TotalImageRecords = 0;
  TotalVideoRecords = 0;
  PageStart = 0;
  PageSize = this.PageSizeUnit;
  ngUnsubscribe: Subject<any> = new Subject<any>();

  constructor(private router: Router,
    private ref: ChangeDetectorRef,
    private zone: NgZone,
    private mediaLibraryService: MediaLibraryService,
    private ngxSmartModalService: NgxSmartModalService,
    public commonFunctionsService: CommonFunctionsService,
  ) {
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os')
  }

  ngOnInit() {
    this.isLoading = true;

    this.getMediaLibraryTypeList();

    this.getMediaLibraryList(0, true);
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getMediaLibraryTypeList() {
    this.mediaLibraryService.GetMediaLibraryTypeList()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.mediaLibraryTypes = res;
        //add default dropdown option for media library types
        this.mediaLibraryTypes.unshift(this.selectedMediaLibraryType);
      });
  }

  getMediaLibraryList(pageIndex: number = 0, isPageLoad: boolean = false) {
    this.isLoading = true;
    this.mediaLibraryService.GetMediaLibraryList(this.selectedMediaLibraryType.Id, pageIndex, this.PageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.TotalRecords = res.TotalCount;
        this.TotalImageRecords = res.ImageCount;
        this.TotalVideoRecords = res.VideoCount;
        this.PageStart = pageIndex + 1;
        this.mediaLibraries = res.MediaLibraryList;

        this.mediaLibraries.forEach(element => {
          const found = this.TrashMediaLibraries.find(
            x => x.Id === element.Id
          );
          if (found !== undefined) {
            element.Selected = true;
          }

          element.CreatedDatetime = new Date(element.CreatedDatetime + 'Z');
        });

        const list = this.mediaLibraries.filter(x => x.Selected == true);
        this.checkAllMedia = list.length === this.PageSize ? true : false;
        this.checkedMedia = list.length > 0 ? true : false;
        this.isLoading = false;

        // if(isPageLoad){
        //   this.getMediaLibraryTypeList();
        // }
      }, (error: any) => {
        this.isLoading = false;
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  setPage(page) {
    this.getMediaLibraryList(page)
  }

  setPageSize(pageSize) {
    this.PageSize = pageSize;
    this.getMediaLibraryList();
  }

  isVideoType(id: number) {
    let count = this.mediaLibraryTypes.filter(x => x.Id === id && String(x.Name).toLocaleLowerCase().indexOf('video') > -1).length;

    return count > 0;
  }

  zoomMedia(media) {
    var index = this.mediaLibraries.map(c => c.Id).indexOf(media.Id);
    this.hideShowPrevNextBtns(index, media);
    this.zoomedMedia = media;

    this.ngxSmartModalService.setModalData(media, 'zoomedMediaModal');
    this.ngxSmartModalService.getModal('zoomedMediaModal').open();
  }

  onClickedOutsideZoomMediaModal(event) {
    this.ngxSmartModalService.resetModalData('zoomedMediaModal');
  }

  previousMedia(media) {
    if (media != undefined) {
      var index = this.mediaLibraries.map(c => c.Id).indexOf(media.Id);
      if (index != 0) {
        //this.zoomedMedia = this.mediaLibraries[index - 1];
        this.ngxSmartModalService.resetModalData('zoomedMediaModal')
        this.ngxSmartModalService.setModalData(this.mediaLibraries[index - 1], 'zoomedMediaModal');
      }
      this.hideShowPrevNextBtns(--index, media);
    }
  }
  nextMedia(media) {
    if (media != undefined) {
      var index = this.mediaLibraries.map(c => c.Id).indexOf(media.Id);
      if (index + 1 < this.mediaLibraries.length) {
        //this.zoomedMedia = this.mediaLibraries[index + 1];
        this.ngxSmartModalService.resetModalData('zoomedMediaModal');
        this.ngxSmartModalService.setModalData(this.mediaLibraries[index + 1], 'zoomedMediaModal');
      }
      this.hideShowPrevNextBtns(++index, media);
    }
  }

  hideShowPrevNextBtns(index: number, media: any) {
    this.hidePrevZoomMediaBtn = false;
    this.hideNextZoomMediaBtn = false;

    if (index <= 0) {
      this.hidePrevZoomMediaBtn = true;
    }
    else if (index >= this.mediaLibraries.length - 1) {
      this.hideNextZoomMediaBtn = true;
    }
  }

  checkMedia(event, media) {
    media.Selected = event.target.checked;
    if (event.target.checked) {
      this.checkedMedia = true;
    }
    else {
      this.TrashMediaLibraries = this.TrashMediaLibraries.filter(x => x.Id != media.Id);
      const selected = this.mediaLibraries.filter(x => x.Selected);
      this.checkedMedia = selected.length > 0 ? true : false;
    }
    this.AddItemsToTrash();
    //this.checkedMedia = !this.checkedMedia;
    //media.Selected = !this.checkedMedia;
  }
  hasCheckedMedia() {
    return this.mediaLibraries.filter(m => m.Selected).length > 0;
  }
  hasCheckedMediaAcrossPages() {
    return this.TrashMediaLibraries.filter(m => m.Selected).length > 0;
  }
  mediaCheckAll(event) {

    this.mediaLibraries.forEach(c => {
      c.Selected = event.target.checked;
    });

    if (!event.target.checked) {
      this.checkedMedia = false;
      this.mediaLibraries.forEach(element => {
        this.TrashMediaLibraries = this.TrashMediaLibraries.filter(
          x => x.Id != element.Id
        );
      });
    }
    this.AddItemsToTrash();
  }
  mediaCheckedLength() {
    return this.TrashMediaLibraries.filter(m => m.Selected).length;
  }

  removeMedia(media) {

    let mediaIdToRemove: number[] = [];
    mediaIdToRemove.push(media.Id as number);

    this.mediaLibraryService.RemoveMedia(mediaIdToRemove)
      .subscribe(res => {
        this.mediaLibraries = this.mediaLibraries.filter(c => c.Id != media.Id);

        const index = this.TrashMediaLibraries.map(c => c.Id).indexOf(media.Id)

        //remove from the list on individual media remove
        if (index !== undefined) {
          this.TrashMediaLibraries.splice(index, 1)
        }

        this.commonFunctionsService.displaySuccessMessage('Media removed successfully!');
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }

  removeAllCheckedMedia() {
    const mediaIdsToRemove: Array<number> = this.TrashMediaLibraries.map(x => x.Id);
    this.mediaLibraryService.RemoveMedia(mediaIdsToRemove)
      .subscribe(res => {
        //remove previously selected items
        this.mediaLibraries = this.mediaLibraries.filter(c => !c.Selected);

        //empty thrash array as all items are processed
        this.TrashMediaLibraries = [];
        this.commonFunctionsService.displaySuccessMessage('Media removed successfully!');
        this.getMediaLibraryList();
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }

  AddItemsToTrash() {
    const selected = this.mediaLibraries.filter(x => x.Selected);
    if (this.TrashMediaLibraries.length > 0) {
      this.mediaLibraries.forEach(element => {
        const found = this.TrashMediaLibraries.find(
          x => x.Id === element.Id
        );
        if (found === undefined) {
          if (element.Selected) {
            this.TrashMediaLibraries.push(element);
          }
        }
      });
    } else {
      this.TrashMediaLibraries = selected;
    }
  }

}