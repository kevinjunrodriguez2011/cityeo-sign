import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaLibraryService } from '../../../services/media-library.service';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { ApiError } from '../../../models/ApiError';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-add-media',
  templateUrl: './add-media.component.html',
  styleUrls: ['./add-media.component.css']
})
export class AddMediaComponent implements OnInit, OnDestroy {
  @ViewChild('fileBrowse') fileBrowseElement: ElementRef;
  @ViewChild('editfileBrowse') editfileBrowseElement: ElementRef;
  fileDragged = false;
  public uploadedMedia = [];
  browserType: string;
  os: string;
  mediaLibraryId: number = 0;
  mediaLibraryDetails = {};
  hasImageShownOnPageLoad = false;
  ngUnsubscribe: Subject<any> = new Subject<any>();
  
  constructor(private router: Router,
    private changeDetector: ChangeDetectorRef,
    private mediaLibraryService: MediaLibraryService,
    private route: ActivatedRoute,
    public commonFunctionsService: CommonFunctionsService
  ) {
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os');
  }
  
  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.mediaLibraryId = params['id']
        this.getMediaLibraryDetails();
      }
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getMediaLibraryDetails() {
    
    if (this.mediaLibraryId > 0) {
      this.mediaLibraryService.GetMediaLibraryDetails(this.mediaLibraryId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        if (res != null && res.Id > 0) {
          this.uploadedMedia = [];
          
          this.mediaLibraryDetails = res;

          this.uploadedMedia.push({
            'FileName': res.Title,
            'FileSize': this.commonFunctionsService.getFileSizeOnHighUnit(res.FileSize) + ' ' + this.commonFunctionsService.getFileSizeUnit(res.FileSize),
            'FileType': res.MIMEType,
            'FileUrl': res.ThumbnailUrl,
            'FileProgress': 100,
            'FileProgessSize': this.commonFunctionsService.getFileSizeOnHighUnit(res.FileSize) + ' ' + this.commonFunctionsService.getFileSizeUnit(res.FileSize),
          });

          this.hasImageShownOnPageLoad = true;
        }
        else {
          this.backToMedia();
        }
      }, (error: any) => {
        this.commonFunctionsService.displayErrorMessage();
        this.backToMedia();
      });
    }
    else {
      this.backToMedia();
    }
  }

  backToMedia() {
    this.router.navigate(['/media-library'])
  }
  fileDropHandler(event) {
    event.preventDefault();
    if (event.dataTransfer.items) {
      this.fileDragged = true;
      let isFileTypeValid = true;
      let isFileSizeValid = true;
      if (this.mediaLibraryId > 0 && event.dataTransfer.files.length > 1) {
        this.fileDragged = false;
        this.commonFunctionsService.displayErrorMessage('Only one image or video can be allowed to replace existing media.');
        return;
      }
      for (var i = 0; i < event.dataTransfer.files.length; i++) {
        var file = event.dataTransfer.items[i].webkitGetAsEntry();
        if (file.isFile) {
          this.fileDragged = false;
          const fileEntry = file;
          fileEntry.file((file: File) => {
            if (this.commonFunctionsService.validateFileType(file.type)) {
              if (this.commonFunctionsService.validateFileSize(file.size)) {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = (event: any) => {
                  //pop old image from the array before pushing if it is edit media
                  if (this.mediaLibraryId > 0)
                    this.uploadedMedia.pop();

                  this.uploadedMedia.push({
                    'FileName': file.name,
                    'FileSize': this.commonFunctionsService.getFileSizeOnHighUnit(file.size) + ' ' + this.commonFunctionsService.getFileSizeUnit(file.size),
                    'FileType': file.type,
                    'FileUrl': event.target.result,
                    'FileProgress': 0,
                  })
                  this.startProgress(file);
                }
              }
              else {
                isFileSizeValid = false;
              }
            }
            else {
              isFileTypeValid = false;
            }
          })
        }
      }
      if (!isFileTypeValid) {
        this.commonFunctionsService.displayErrorMessage('Only images and videos are allowed to upload. Uploaded images should contain extensions such as .jpeg, .jpg or .png');
      }
      else if (!isFileSizeValid) {
        this.commonFunctionsService.displayErrorMessage('Invalid file size. Please upload images or videos with file size up to 50MB.');
      }

      setTimeout(() => {
        this.changeDetector.detectChanges();
      }, 10);
    }
  }
  removeImage(file) {
    if (file.Id !== undefined && file.Id > 0) {
      let mediaIdToRemove: number[] = [];
      mediaIdToRemove.push(file.Id as number);

      this.mediaLibraryService.RemoveMedia(mediaIdToRemove)
      .subscribe(res => {

        this.uploadedMedia = this.uploadedMedia.filter(u => u.Id != file.Id);
        setTimeout(() => {
          this.changeDetector.detectChanges();
        }, 10);
        this.commonFunctionsService.displaySuccessMessage('Media removed successfully!');
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
    }
    else {
      this.uploadedMedia = this.uploadedMedia.filter(u => u.FileName != file.FileName);
      setTimeout(() => {
        this.changeDetector.detectChanges();
      }, 10);
    }

  }
  onFileBrowse(files) {
    this.processFiles(files);
  }
  processFiles(files) {
    let isFileTypeValid = true;
    let isFileSizeValid = true;
    for (const file of files) {
      if (this.commonFunctionsService.validateFileType(file.type)) {
        if (this.commonFunctionsService.validateFileSize(file.size)) {
          var reader = new FileReader();
          reader.readAsDataURL(file); // read file as data url
          reader.onload = (event: any) => { // called once readAsDataURL is completed

            //pop old image from the array before pushing if it is edit media
            if (this.mediaLibraryId > 0)
              this.uploadedMedia.pop();

            this.uploadedMedia.push({
              'FileName': file.name,
              'FileSize': this.commonFunctionsService.getFileSizeOnHighUnit(file.size) + ' ' + this.commonFunctionsService.getFileSizeUnit(file.size),
              'FileType': file.type,
              'FileUrl': event.target.result,
              'FileProgessSize': 0,
              'FileProgress': 0,
            })

            this.startProgress(file);
          }
        }
        else {
          isFileSizeValid = false;
        }
      }
      else {
        isFileTypeValid = false;
      }
    }

    if (!isFileTypeValid) {
      this.commonFunctionsService.displayErrorMessage('Only images and videos are allowed to upload. Uploaded images should contain extensions such as .jpeg, .jpg or .png');
    }
    else if (!isFileSizeValid) {
      this.commonFunctionsService.displayErrorMessage('Invalid file size. Please upload images or videos with file size up to 50MB.');
    }

    this.fileBrowseElement.nativeElement.value = "";
    this.editfileBrowseElement.nativeElement.value = "";
  }
  enableDrop(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.fileDragged = true;
  }
  fileDragLeave(ev) {
    this.fileDragged = false;
  }

  async startProgress(file) {
    let filteredFile = this.uploadedMedia.filter(u => u.FileName === file.name && (!u.hasOwnProperty('Id') || u.Id === undefined)).pop();
    filteredFile.hasError = false;
    let fileSize = this.commonFunctionsService.getFileSizeOnHighUnit(file.size);
    let fileSizeInWords = this.commonFunctionsService.getFileSizeUnit(file.size);

    let formData = new FormData();
    formData.append('File', file);
    formData.append('MediaLibraryId', String(this.mediaLibraryId));

    this.mediaLibraryService.UploadMedia(formData)
    .subscribe(res => {
      if (res.status === 'progress') {
        let completedPercentage = parseFloat(res.message);
        completedPercentage = completedPercentage == 100 ? 99 : completedPercentage;
        filteredFile.FileProgessSize = `${(fileSize * completedPercentage / 100).toFixed(2)} ${fileSizeInWords}`;
        filteredFile.FileProgress = completedPercentage;
        this.changeDetector.detectChanges();
      }
      else if (res.status === 'completed') {
        filteredFile.Id = res.Id;
        if (file.type.indexOf('video') > -1)
          filteredFile.FileUrl = res.ThumbnailUrl;
        filteredFile.ThumbnailUrl = res.ThumbnailUrl;

        filteredFile.FileProgessSize = fileSize + ' ' + fileSizeInWords;
        filteredFile.FileProgress = 100;

        this.hasImageShownOnPageLoad = false;
        this.changeDetector.detectChanges();
        //this.commonFunctionsService.displaySuccessMessage('Media uploaded successfully!');
      }
      else if (res.status === 'notAllowed') {
        this.removeImage(file);
        this.commonFunctionsService.displayErrorMessage('Only images and videos are allowed to upload. Uploaded images should contain extensions such as .jpeg, .jpg or .png');
      }
    }, (error: any) => {
      filteredFile.hasError = true;
      // const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

      // if(apiError !== undefined && apiError !=null)
      //   this.commonFunctionsService.displayErrorMessage(error.error.error_description);
      // else
      //   this.commonFunctionsService.displayErrorMessage();
    });
  }
  fakeWaiter(ms: number) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
}
