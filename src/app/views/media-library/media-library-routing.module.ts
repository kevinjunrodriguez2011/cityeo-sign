import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { AddMediaComponent } from './add-media/add-media.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  },
  {
    path: 'add-media',
    component: AddMediaComponent,
  },
  {
    path: 'edit-media/:id',
    component: AddMediaComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MediaLibraryRoutingModule { }
