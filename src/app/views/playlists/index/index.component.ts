import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PlaylistService } from '../../../services/playlist.service';
import { IPlaylistList } from '../../../interfaces/IPlaylistList';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { ApiError } from '../../../models/ApiError';
import { ModalService } from '../../../services/modal.service';
import { PlaylistPreviewComponent } from '../../modal-templates/playlist-preview/playlist-preview.component';
import { NotifyEventService } from '../../../services/notify-event.service';
import { PlaylistScheduleModalComponent } from '../../modal-templates/playlist-schedule-modal/playlist-schedule-modal.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit, OnDestroy {

  checkAllPlayLists: boolean = false;
  checkedPlayList: boolean = false;
  browserType: string;
  os: string;
  isLoading: boolean = true;
  PageSizeUnit = 25;
  TotalRecords = 0;
  PageStart = 0;
  PageSize = this.PageSizeUnit;

  playlistList: Array<IPlaylistList> = [];
  TrashPlaylistList: Array<IPlaylistList> = [];
  ngUnsubscribe: Subject<any> = new Subject<any>();

  constructor(private router: Router,
    private route: ActivatedRoute,
    private playlistService: PlaylistService,
    private notifyEventService: NotifyEventService,
    private modalService: ModalService,
    public commonFunctionsService: CommonFunctionsService) {
    this.browserType = localStorage.getItem('browser');
    this.os = localStorage.getItem('os');
  }

  ngOnInit() {
    this.isLoading = true;
    this.getPlaylistList();

    this.notifyEventService.playlistSetScheduleEvent$
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        //keep user on the same page 
        this.getPlaylistList(this.PageStart - 1);
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  getPlaylistList(pageIndex: number = 0) {
    this.isLoading = true;
    this.playlistService.getPlaylists(pageIndex, this.PageSize)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.TotalRecords = res.TotalCount;
        this.PageStart = pageIndex + 1;
        this.playlistList = res.PlayLists;

        this.playlistList.forEach(element => {
          const found = this.TrashPlaylistList.find(
            x => x.Id === element.Id
          );
          if (found !== undefined) {
            element.Checked = true;
          }

          element.ModifiedDatetime = new Date(element.ModifiedDatetime + 'Z');

          element.StartTime = this.commonFunctionsService.formatTimeAMPM(element.StartTimeMinutes);
          element.EndTime = this.commonFunctionsService.formatTimeAMPM(element.EndTimeMinutes);
        });

        const list = this.playlistList.filter(x => x.Checked == true);
        this.checkAllPlayLists = list.length === this.PageSize ? true : false;
        this.checkedPlayList = list.length > 0 ? true : false;
        this.isLoading = false;
      }, (error: any) => {
        this.isLoading = false;
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  setPage(page) {
    this.getPlaylistList(page)
  }

  setPageSize(pageSize) {
    this.PageSize = pageSize;
    this.getPlaylistList(0)
  }

  checkPlaylist(event, media) {
    media.Checked = event.target.checked;
    if (event.target.checked) {
      this.checkedPlayList = true;
    }
    else {
      this.TrashPlaylistList = this.TrashPlaylistList.filter(x => x.Id != media.Id);
      const selected = this.playlistList.filter(x => x.Checked);
      this.checkedPlayList = selected.length > 0 ? true : false;
    }
    this.AddItemsToTrash();
    //this.checkedPlayList = !this.checkedPlayList;
    //media.Checked = !this.checkedPlayList;
  }

  hasCheckedPlaylist() {
    return this.playlistList.filter(d => d.Checked).length > 0;
  }

  hasCheckedPlaylistAcrossPages() {
    return this.TrashPlaylistList.filter(m => m.Checked).length > 0;
  }

  playlistCheckAll(event) {
    this.playlistList.forEach(c => {
      c.Checked = event.target.checked;
    });

    if (!event.target.checked) {
      this.checkedPlayList = false;
      this.playlistList.forEach(element => {
        this.TrashPlaylistList = this.TrashPlaylistList.filter(
          x => x.Id != element.Id
        );
      });
    }
    this.AddItemsToTrash();
  }
  playlistCheckedLength() {
    return this.TrashPlaylistList.filter(m => m.Checked).length;
  }
  removePlaylist(playlist) {
    let playListIdToRemove: number[] = [];
    playListIdToRemove.push(playlist.Id as number);

    this.playlistService.RemovePlayList(playListIdToRemove)
      .subscribe((res: any) => {
        this.playlistList = this.playlistList.filter(c => c.Id != playlist.Id);

        const index = this.TrashPlaylistList.map(c => c.Id).indexOf(playlist.Id)

        //remove from the list on individual media remove
        if (index !== undefined) {
          this.TrashPlaylistList.splice(index, 1)
        }

        this.commonFunctionsService.displaySuccessMessage('Playlist removed successfully!');
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }

  removeAllCheckedPlaylist() {
    const playlistIdsToRemove: number[] = this.TrashPlaylistList.map(x => x.Id);
    this.playlistService.RemovePlayList(playlistIdsToRemove)
      .subscribe((res: any) => {
        //remove previously selected items
        this.playlistList = this.playlistList.filter(c => !c.Checked);

        //empty thrash array as all items are processed
        this.TrashPlaylistList = [];
        this.commonFunctionsService.displaySuccessMessage('Playlist removed successfully!');
        this.getPlaylistList();
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }

  AddItemsToTrash() {
    const selected = this.playlistList.filter(x => x.Checked);
    if (this.TrashPlaylistList.length > 0) {
      this.playlistList.forEach(element => {
        const found = this.TrashPlaylistList.find(
          x => x.Id === element.Id
        );
        if (found === undefined) {
          if (element.Checked) {
            this.TrashPlaylistList.push(element);
          }
        }
      });
    } else {
      this.TrashPlaylistList = selected;
    }
  }

  previewPlayList(playlist) {
    this.playlistService.getPlaylistDetails(playlist.Id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        if (res != null && res.Id > 0) {
          if (res.PlayListItems != undefined && res.PlayListItems != null && res.PlayListItems.length > 0) {
            let medialibraries = res.PlayListItems.map(x => x.MediaLibrary);

            medialibraries.forEach(element => {
              if (element.Duration === undefined || element.Duration == null)
                element.Duration = this.commonFunctionsService.defaultMediaDuration;
            });
            
            let frameWidth, frameHeight;

            if(res.DisplaySize != null && res.DisplaySize.Id > 0){
              frameWidth = res.DisplaySize.FrameWidth;
              frameHeight = res.DisplaySize.FrameHeight;
            }

            const initialState = {
              playlistMedia: [...medialibraries],
              isPortrait: res.IsPortrait,
              frameWidth: frameWidth,
              frameHeight: frameHeight
            };

            this.modalService.openModal(PlaylistPreviewComponent, { initialState });
          }
        }

      }, (error: any) => {
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  openSchedulePopup(playlist) {
    const initialState = {
      playlist: { ...playlist }
    };

    this.modalService.openModal(PlaylistScheduleModalComponent, { initialState });
  }
}
