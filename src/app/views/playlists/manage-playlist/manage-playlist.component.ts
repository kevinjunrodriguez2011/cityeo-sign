import { Component, OnInit, NgZone, ChangeDetectorRef, ViewChild, ChangeDetectionStrategy, DoCheck } from '@angular/core';
import { Router, NavigationEnd, RoutesRecognized, ActivatedRoute } from '@angular/router';
import { NotifyEventService } from '../../../services/notify-event.service';
import { SortableSpec, DraggedItem } from "@angular-skyhook/sortable";
import { ModalService } from '../../../services/modal.service';
import { ControlPanelModalComponent } from '../../modal-templates/control-panel-modal/control-panel-modal.component';
import { PlaylistService } from '../../../services/playlist.service';
import { MediaLibrary } from '../../../models/MediaLibrary';
import { CommonFunctionsService } from '../../../services/common-functions.service';
import { ApiError } from '../../../models/ApiError';
import { PlaylistPreviewComponent } from '../../modal-templates/playlist-preview/playlist-preview.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-manage-playlist',
  templateUrl: './manage-playlist.component.html',
  styleUrls: ['./manage-playlist.component.css']
})
export class ManagePlaylistComponent implements OnInit {
  playlistId: number = 0;
  managePlaylistName = 'Untitled Playlist Name';
  editManagePlaylist = false;
  defaultPlaylistItem: MediaLibrary = {
    Id: 0,
    Title: 'for-add-only',
    MediaLibraryTypeId: undefined,
    FrameWidth: undefined,
    FrameHeight: undefined,
    Duration: undefined,
    FileSize: undefined,
    MIMEType: undefined,
    Url: undefined,
    ThumbnailUrl: undefined,
    CreatedDatetime: undefined,
    Selected: false
  };
  selectedItem: { Idx: number, mediaLibrary: MediaLibrary };
  playlist: MediaLibrary[] = [];
  playlistTemp: MediaLibrary[] = [];
  
  playlistForm: any;
  isShowOrientationSelectList = false;
  isShowDisplaySizeSelectList = false;
  isShowScheduleSelectList = false;
  defaultOrientation = 'Select Orientation';
  defaultDisplaySize = 'Select Display Size';
  defaultSchedule = 'Set Schedule'
  selectedOrientation = this.defaultOrientation;
  selectedDisplaySize = this.defaultDisplaySize;
  selectedSchedule = this.defaultSchedule;
  isPortrait: boolean = false;
  isLandscape: boolean = false;
  isLoadingDisplaySizes = false;
  displaySizeList = [];

  playlistSchedule: any = {
    IsEveryday: true,
    IsForFullDay: true,
    Days: null,
    StartTime: null,
    EndTime: null
  };
  scheduleFormGroupKey = 'Schedule';
  hasReceivedScheduleFormGroup = false;

  get Schedule() { return this.playlistForm.get(this.scheduleFormGroupKey) }
  get ScheduleIsEveryday() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'IsEveryday'); }
  get ScheduleIsForFullDay() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'IsForFullDay'); }
  get ScheduleTheseDays() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'TheseDays'); }
  get ScheduleThisTime() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'ThisTime'); }
  get ScheduleTheseDaysCheckBoxGroup() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'TheseDaysCheckBoxGroup') as FormArray; }
  get ScheduleStartTime() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'StartTime'); }
  get ScheduleEndTime() { return this.playlistForm.get(this.scheduleFormGroupKey + '.' + 'EndTime'); }

  ngUnsubscribe: Subject<any> = new Subject<any>();

  constructor(private notifyEventService: NotifyEventService,
    private formBuilder: FormBuilder,
    private modalService: ModalService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private playlistService: PlaylistService,
    public commonFunctionsService: CommonFunctionsService) { }

  ngOnInit() {
    this.resetPlaylist();
    this.buildForm();
    this.getdisplaySizeList();

    this.route.params.subscribe(params => {
      if (params['id'] !== undefined) {
        this.playlistId = params['id'];
        this.getPlaylistDetails();
      }
    });

    this.notifyEventService.addMediaEvent$
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(medias => {
        this.addPlaylist(medias);
      });

    this.playlistTemp = this.playlist;
  }

  buildForm(){
    this.playlistForm = this.formBuilder.group({
    });
  }

  formInitialized(name: string, form: FormGroup) {
    this.playlistForm.setControl(name, form);
    this.hasReceivedScheduleFormGroup = true;
    this.Schedule.updateValueAndValidity();
  }

  getdisplaySizeList() {
    this.isLoadingDisplaySizes = true;

    this.playlistService.getDisplaySizeList()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((res: any) => {
        this.isLoadingDisplaySizes = false;
        this.displaySizeList = res;
      }, (error: any) => {
        this.isLoadingDisplaySizes = false;
        this.commonFunctionsService.displayErrorMessage();
      });
  }

  getPlaylistDetails() {
    this.resetPlaylist();

    if (this.playlistId > 0) {
      this.playlistService.getPlaylistDetails(this.playlistId)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((res: any) => {
          if (res != null && res.Id > 0) {
            this.managePlaylistName = res.Name;
            
            if(res.IsPortrait !=null){
              this.isPortrait = res.IsPortrait;
              this.isLandscape = !res.IsPortrait;
            }
            else{
              this.isPortrait = false;
              this.isLandscape = false;
            }

            this.playlistSchedule = {
              IsEveryday: res.IsEveryday,
              IsForFullDay: res.IsForFullDay,
              Days: res.Days,
              StartTimeMinutes: res.StartTimeMinutes,
              EndTimeMinutes: res.EndTimeMinutes
            };
            this.hasReceivedScheduleFormGroup = false;

            if(res.IsPortrait !=null)
              this.selectedOrientation = res.IsPortrait ? 'Portrait' : 'Landscape';
            else
              this.selectedOrientation = this.defaultOrientation;

            if(res.DisplaySize != null && res.DisplaySize.Id > 0)
              this.selectedDisplaySize = res.DisplaySize.Name;
            else
              this.selectedDisplaySize = this.defaultDisplaySize;

            this.displaySizeList.forEach(element => {
                if(element.Id == res.DisplaySize.Id)
                  element.Checked = true;
                else
                  element.Checked = false;
            });

            this.setScheduleSelectedText();

            if (res.PlayListItems != undefined && res.PlayListItems != null && res.PlayListItems.length > 0) {
              let medialibraries = res.PlayListItems.map(x => x.MediaLibrary);

              medialibraries.forEach(element => {

                if (element.Duration === undefined || element.Duration == null)
                  element.Duration = this.commonFunctionsService.defaultMediaDuration;
              });

              this.playlist.push(...medialibraries);
              let forAdd = this.playlist.filter(p => p.Id === 0 && p.Title === 'for-add-only')[0];
              this.playlist = this.playlist.filter(p => p.Id !== 0 && p.Title !== 'for-add-only')
              this.playlist.push(forAdd);

              this.playlistTemp = this.playlist;

              this.managePlaylistSpec.endDrag(null);
            }
          }
          else {
            this.backToPlaylists();
          }
        }, (error: any) => {
          this.commonFunctionsService.displayErrorMessage();
          this.backToPlaylists();
        });
    }
    else {
      this.backToPlaylists();
    }
  }

  resetPlaylist() {
    this.playlist = [];
    this.playlist = this.playlist.concat(...[this.defaultPlaylistItem]);
  }

  backToPlaylists() {
    this.router.navigate(['/playlists']);
  }

  toggleOrientationSelectlist() {
    this.isShowOrientationSelectList = !this.isShowOrientationSelectList;
  }

  toggleDisplaySizeSelectlist() {
    this.isShowDisplaySizeSelectList = !this.isShowDisplaySizeSelectList;
  }

  toggleScheduleSelectlist(){
    this.isShowScheduleSelectList = !this.isShowScheduleSelectList;

    if(!this.isShowScheduleSelectList){
      this.setSchedule();
    }
  }

  closeScheduleSelectlist(){
    this.isShowScheduleSelectList = false;
    this.setSchedule();
  }

  setSchedule(){
    let selectedDays = this.ScheduleTheseDaysCheckBoxGroup.controls
        .filter(control => control.value.Checked)
        .map(days =>
          days.value.Id
        ).join(',');
      
      if(selectedDays == null || selectedDays == ''){
        this.ScheduleIsEveryday.setValue(true);
      }

      if(this.ScheduleStartTime.value == null 
        || this.ScheduleEndTime.value == null
        || this.ScheduleStartTime.value == '' 
        || this.ScheduleEndTime.value == ''
        ){
        this.ScheduleIsForFullDay.setValue(true);
      }

      this.playlistSchedule = {
        IsEveryday: this.ScheduleIsEveryday.value,
        IsForFullDay: this.ScheduleIsForFullDay.value,
        Days: selectedDays,
        StartTime: this.ScheduleStartTime.value,
        EndTime: this.ScheduleEndTime.value
      };
      this.hasReceivedScheduleFormGroup = false;
      this.setScheduleSelectedText();
  }

  setScheduleSelectedText(){
    if(this.playlistSchedule !=null) {
      if(this.playlistSchedule.IsEveryday) {
        if(this.playlistSchedule.IsForFullDay) {
          this.selectedSchedule = '24 / 7';
        }
        else {
          this.selectedSchedule = `Every Day `;
          if(this.playlistSchedule.StartTime === undefined || this.playlistSchedule.StartTime === null)
            this.selectedSchedule +=`(${this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.StartTimeMinutes)} - ${this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.EndTimeMinutes)})`;
          else
            this.selectedSchedule += `(${this.playlistSchedule.StartTime} - ${this.playlistSchedule.EndTime})`;
        }
      }
      else {
        this.selectedSchedule = this.commonFunctionsService.formatDaysFromNumbers(this.playlistSchedule.Days) + ' ';

        if(!this.playlistSchedule.IsForFullDay) {
          if(this.playlistSchedule.StartTime === undefined || this.playlistSchedule.StartTime === null)
            this.selectedSchedule +=`(${this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.StartTimeMinutes)} - ${this.commonFunctionsService.formatTimeAMPM(this.playlistSchedule.EndTimeMinutes)})`;
          else
            this.selectedSchedule += `(${this.playlistSchedule.StartTime} - ${this.playlistSchedule.EndTime})`;
        }
      }
    }
  }

  changeDisplaySizeMenu(event, selectedIndex) {
    let selectedDisplaySizeList = this.displaySizeList.filter((value, index) => index === selectedIndex);
    selectedDisplaySizeList[0].Checked = event.target.checked;

    if (event.target.checked) {
      let filteredDisplaySizeList = this.displaySizeList.filter((value, index) => index !== selectedIndex && value.Checked);
      filteredDisplaySizeList.forEach((element) => {
        element.Checked = false;
      });
    }

    this.setDisplaySizeText();
    this.isShowDisplaySizeSelectList = !this.isShowDisplaySizeSelectList;
  }

  setDisplaySizeText() {
    let displaySize = this.displaySizeList.filter(control => control.Checked)
    this.selectedDisplaySize = displaySize.length > 0 ? displaySize[0].Name : this.defaultDisplaySize;
  }

  changeOrientationMenu(event, isPortrait) {
    if (event.target.checked) {
      this.isPortrait = isPortrait;
      this.isLandscape = !isPortrait;
      this.selectedOrientation = isPortrait ? 'Portrait' : 'Landscape';
    }
    else {
      this.isPortrait = false;
      this.isLandscape = false;
      this.selectedOrientation = this.defaultOrientation;
    }

    this.isShowOrientationSelectList = false;
  }

  move(item: DraggedItem<MediaLibrary>) {
    // shallow clone the list
    // do this so we can avoid overwriting our 'saved' list.
    const temp = this.playlist.slice(0);
    // delete where it was previously
    temp.splice(item.index, 1);
    // add it back in at the new location
    temp.splice(item.hover.index, 0, item.data);
    return temp;
  }
  managePlaylistSpec: SortableSpec<MediaLibrary> = {
    type: "PRIORITY",
    // trackBy is required
    trackBy: x => x.Id,
    hover: item => {
      this.playlistTemp = this.move(item)
    },
    drop: item => { // save the changes
      this.playlistTemp = this.playlist = this.move(item);
    },
    endDrag: _item => { // revert
      this.playlistTemp = this.playlist;
    }
  }
  onEditManagePlaylistName() {
    this.editManagePlaylist = !this.editManagePlaylist;
  }
  exitManagePlaylistNameEdit() {
    this.editManagePlaylist = false;
    if (this.managePlaylistName === '') {
      this.managePlaylistName = 'Untitled Media Playlist'
    }
    this.notifyEventService.managePlaylistName = this.managePlaylistName;
  }
  openModal() {
    this.playlistService.selectedMedias = [];
    this.selectedItem = {
      Idx: undefined,
      mediaLibrary: undefined
    };

    const initialState = {
      selectedItem: this.selectedItem
    };

    this.modalService.openModal(ControlPanelModalComponent, { initialState });
  }
  onOpenAddMediaModal(media, selectedIdx) {
    this.playlistService.selectedMedias = [];

    if (media) {
      this.selectedItem = {
        Idx: selectedIdx,
        mediaLibrary: { ...media }
      };
    }

    const initialState = {
      selectedItem: this.selectedItem
    };

    this.modalService.openModal(ControlPanelModalComponent, { initialState });
  }
  addPlaylist(playlist: MediaLibrary[]) {
    if (playlist != undefined && playlist != null && playlist.length > 0) {
      let isMultipleSelected = playlist.length > 1;
      let hasPreviousSelection = !(this.selectedItem.mediaLibrary === undefined || this.selectedItem.Idx === undefined);

      //check if any item was selected before showing the modal to select items
      if (hasPreviousSelection) {
        let index = playlist.findIndex(x => x.Id == this.selectedItem.mediaLibrary.Id);

        //check if multiple items are selected from the modal
        if (isMultipleSelected) {

          //check if previously selected item is also selected from the modal
          if (index > -1) {
            this.playlist[this.selectedItem.Idx].Duration = playlist[index].Duration;

            //remove previously selected item from the parameter array
            //so it won't be added again since it is edit
            playlist.splice(index, 1);
          }
        }
        else {
          //replace new item with that in the current list
          Object.assign(this.playlist[this.selectedItem.Idx], playlist[0]);
        }
      }

      //if no items previously selected or more than one items are selected from the list
      if (isMultipleSelected || !hasPreviousSelection) {
        //add rest of the items in the final selection list
        this.playlist.push(...playlist);
        let forAdd = this.playlist.filter(p => p.Id === 0 && p.Title === 'for-add-only')[0];
        this.playlist = this.playlist.filter(p => p.Id !== 0 && p.Title !== 'for-add-only')
        this.playlist.push(forAdd);
      }
      this.playlistTemp = this.playlist;

      this.managePlaylistSpec.endDrag(null);
    }
  }
  removePlaylist(Id, index) {
    this.playlist.splice(index, 1);
    //this.playlist = this.playlist.filter(p => p.Id !== Id);
    this.managePlaylistSpec.endDrag(null);
  }

  SavePlayList(event) {
    this.setSchedule();

    let selectedDisplaySize = this.displaySizeList.filter(x=>x.Checked);

    let playListSaveModel: { Id: number, Name: string, IsPortrait: boolean, DisplaySizeId: number, PlaylistSchedule: any, PlayListIds: any[] } = {
      Id: this.playlistId,
      Name: this.managePlaylistName,
      IsPortrait: this.isPortrait,
      DisplaySizeId: selectedDisplaySize.length > 0 ? selectedDisplaySize[0].Id: null,
      PlaylistSchedule: {},
      PlayListIds: []
    };

    //set schedule params
    let selectedDays = this.ScheduleTheseDaysCheckBoxGroup.controls
    .filter(control => control.value.Checked)
    .map(days =>
      days.value.Id
    );

    playListSaveModel.PlaylistSchedule.IsEveryday = this.ScheduleIsEveryday.value;
    playListSaveModel.PlaylistSchedule.IsForFullDay = this.ScheduleIsForFullDay.value;
    playListSaveModel.PlaylistSchedule.Days = selectedDays.length > 0 ? selectedDays.join(',') : null;
    playListSaveModel.PlaylistSchedule.StartTime = this.ScheduleStartTime.value;
    playListSaveModel.PlaylistSchedule.EndTime = this.ScheduleEndTime.value;

    if (this.playlist.length > 1) {
      playListSaveModel.PlayListIds = this.playlist.filter(p => p.Id !== 0 && p.Title !== 'for-add-only')
        .map(({ Id, Duration }) => ({ Id, Duration }));
    }

    this.playlistService.SavePlayList(playListSaveModel)
      .subscribe((res: any) => {
        this.commonFunctionsService.displaySuccessMessage('Playlist saved successfully!');
        this.router.navigate(['playlists', 'manage-playlist', res.Id]);
      }, (error: any) => {
        const apiError: ApiError = error.hasOwnProperty('error') ? error.error : null;

        if (apiError !== undefined && apiError != null)
          this.commonFunctionsService.displayErrorMessage(error.error.error_description);
        else
          this.commonFunctionsService.displayErrorMessage();
      });
  }
  hasValidMedia() {
    return this.playlist.filter(p => p.Id !== 0 && p.Title !== 'for-add-only').length > 0
  }
  PreviewPlayList(event) {
    if (this.hasValidMedia()) {
      let frameWidth: number, frameHeight: number;

      let tempPlaylist = this.playlist.filter(p => p.Id !== 0 && p.Title !== 'for-add-only');
      
      let selectedDisplaySize = this.displaySizeList.filter(x=>x.Checked);
      if(selectedDisplaySize.length > 0) {
        frameWidth = selectedDisplaySize[0].FrameWidth;
        frameHeight = selectedDisplaySize[0].FrameHeight;
      }

      const initialState = {
        playlistMedia: [...tempPlaylist],
        isPortrait: this.isPortrait,
        frameWidth: frameWidth,
        frameHeight: frameHeight
      };

      this.modalService.openModal(PlaylistPreviewComponent, { initialState });
    }
  }

  goBack(){
    this.location.back();
  }
}