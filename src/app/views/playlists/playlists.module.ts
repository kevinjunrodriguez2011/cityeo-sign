import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { IndexComponent } from './index/index.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagerModule } from '../pager/pager.module';
import { PopoverModule } from 'ngx-bootstrap';
import { ManagePlaylistComponent } from './manage-playlist/manage-playlist.component';
import { SkyhookSortableModule } from '@angular-skyhook/sortable';
import { SkyhookDndModule } from '@angular-skyhook/core';
import { SkyhookMultiBackendModule } from '@angular-skyhook/multi-backend';
import { customMultiBackend } from '../../customMultiBackend';
import { ClickOutsideModule } from 'ng-click-outside';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    PlaylistsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PagerModule,
    PopoverModule.forRoot(),
    SkyhookSortableModule,
    SkyhookDndModule.forRoot({ backendFactory: customMultiBackend }),
    SkyhookMultiBackendModule,
    ClickOutsideModule,
    SharedModule
  ],
  declarations: [IndexComponent, ManagePlaylistComponent],
  exports: [IndexComponent, ManagePlaylistComponent],
  providers:[]
})
export class PlaylistsModule { }
