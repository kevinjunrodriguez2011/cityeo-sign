import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistScheduleComponent } from './playlist-schedule.component';

describe('PlaylistScheduleComponent', () => {
  let component: PlaylistScheduleComponent;
  let fixture: ComponentFixture<PlaylistScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
