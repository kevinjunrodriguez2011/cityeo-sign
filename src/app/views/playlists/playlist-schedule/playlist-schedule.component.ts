import { Component, OnInit, Input, EventEmitter, Output, SimpleChanges, OnChanges } from '@angular/core';
import { Validators, FormGroup, FormArray, ValidatorFn, FormControl, FormBuilder } from '@angular/forms';
import { CommonFunctionsService } from 'src/app/services/common-functions.service';
import { IPlaylistList } from 'src/app/interfaces/IPlaylistList';

@Component({
  selector: 'app-playlist-schedule',
  templateUrl: './playlist-schedule.component.html',
  styleUrls: ['./playlist-schedule.component.css']
})
export class PlaylistScheduleComponent implements OnInit, OnChanges {
  @Output() formReady = new EventEmitter<FormGroup>();
  @Input() playlistSchedule: IPlaylistList;
  @Input() isVerticalLayout: boolean;
  scheduleForm: FormGroup;
  hoursList = [];
  startTimeVisible = false;
  endTimeVisible = false;
  dayNames: { Id: number, Text: string, Checked: boolean }[] = [];

  get IsEveryday() { return this.scheduleForm.get('IsEveryday'); }
  get IsForFullDay() { return this.scheduleForm.get('IsForFullDay'); }
  get TheseDays() { return this.scheduleForm.get('TheseDays'); }
  get ThisTime() { return this.scheduleForm.get('ThisTime'); }
  get TheseDaysCheckBoxGroup() { return this.scheduleForm.get('TheseDaysCheckBoxGroup') as FormArray; }
  get StartTime() { return this.scheduleForm.get('StartTime'); }
  get EndTime() { return this.scheduleForm.get('EndTime'); }

  constructor(private formBuilder: FormBuilder,
    public commonFunctionsService: CommonFunctionsService) { 
      this.hoursList = this.generateHours();
      this.dayNames = this.commonFunctionsService.playlistScheuleDays.map((value, index) => {
        return { Id: index + 1, Text: value, Checked: false };
      });
  
      this.buildForm();
    }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.playlistSchedule) {
      this.bindFormData(changes.playlistSchedule.currentValue);
    }
  }

  ngOnInit() {
    this.formReady.emit(this.scheduleForm);
  }

  buildForm() {
    let theseDaysCheckBoxGroup = new FormArray(this.dayNames.map(item => new FormGroup({
      Id: new FormControl(item.Id),
      Text: new FormControl(item.Text),
      Checked: new FormControl(item.Checked)
    })));
    let timeRegexPattern = /\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))/;

    this.scheduleForm = this.formBuilder.group({
      'IsEveryday': [true],
      'TheseDays': [false],
      'IsForFullDay': [true],
      'ThisTime': [false],
      'TheseDaysCheckBoxGroup': theseDaysCheckBoxGroup,
      'StartTime': [null, Validators.pattern(/\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))/)],
      'EndTime': [null, Validators.pattern(/\b((1[0-2]|0?[1-9]):([0-5][0-9]) ([AaPp][Mm]))/)],
    });
    this.TheseDays.valueChanges
      .subscribe(theseDays => {
        if (theseDays) {
          this.TheseDaysCheckBoxGroup.setValidators(this.minSelectedCheckboxes(1));
        }
        else {
          this.TheseDaysCheckBoxGroup.setValidators(null);
        }

        this.TheseDaysCheckBoxGroup.updateValueAndValidity();
      });

    this.ThisTime.valueChanges.subscribe(thisTime => {
      if (thisTime) {
        this.StartTime.setValidators([Validators.required, Validators.pattern(timeRegexPattern)]);
        this.EndTime.setValidators([Validators.required, Validators.pattern(timeRegexPattern)]);
      }
      else {
        this.StartTime.setValidators(null);
        this.EndTime.setValidators(null);
      }
      this.StartTime.updateValueAndValidity();
      this.EndTime.updateValueAndValidity();
    });
  }

  bindFormData(playlistSchedule: any) {
    if(playlistSchedule.IsEveryday !== undefined && playlistSchedule.IsForFullDay !== undefined){
      this.IsEveryday.setValue(playlistSchedule.IsEveryday);
      this.TheseDays.setValue(!playlistSchedule.IsEveryday);
      this.IsForFullDay.setValue(playlistSchedule.IsForFullDay);
      this.ThisTime.setValue(!playlistSchedule.IsForFullDay);
  
      if (!playlistSchedule.IsEveryday) {
        if (playlistSchedule.Days !== undefined && playlistSchedule.Days != null) {
          let dayNumbers = playlistSchedule.Days.split(',');
  
          this.TheseDaysCheckBoxGroup.controls.forEach((control: FormGroup) => {
            if (dayNumbers.includes(String(control.controls['Id'].value)))
              control.controls['Checked'].setValue(true);
            else
              control.controls['Checked'].setValue(false);
          });
        }
      }
  
      if (!playlistSchedule.IsForFullDay) {
        if(playlistSchedule.StartTimeMinutes !== undefined)
          this.StartTime.setValue(this.commonFunctionsService.formatTimeAMPM(playlistSchedule.StartTimeMinutes));
        else
          this.StartTime.setValue(playlistSchedule.StartTime);
  
        if(playlistSchedule.EndTimeMinutes !== undefined)
          this.EndTime.setValue(this.commonFunctionsService.formatTimeAMPM(playlistSchedule.EndTimeMinutes));
        else
          this.EndTime.setValue(playlistSchedule.EndTime);
      }
    }
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        // get a list of checkbox values (boolean)
        .map(control => control.value.Checked)
        // total up the number of checked checkboxes
        .reduce((prev, next) => next ? prev + next : prev, 0);

      // if the total is not greater than the minimum, return the error message
      return totalSelected >= min ? null : { required: true };
    };

    return validator;
  }

  onChangeEveryDay(event) {
    this.TheseDays.setValue(!event.target.checked);
  }

  onChangeTheseDays(event) {
    this.IsEveryday.setValue(!event.target.checked);
  }

  onChangeForFullDay(event) {
    this.ThisTime.setValue(!event.target.checked);
  }

  onChangeThisTime(event) {
    this.IsForFullDay.setValue(!event.target.checked);
  }

  generateHours(offset = 0, meridiemSwap = false) {
    let timeList = [];
    let meridiem = meridiemSwap ? 'pm' : 'am';
    for (let hour = offset; hour <= 23 + offset; hour++) {
      let hourDisplay = hour == 0 ? 12 : hour > 12 ? hour - 12 : hour;
      if (hour > 11 && hour < 24) {
        meridiem = meridiemSwap ? 'am' : 'pm';
      } else {
        meridiem = meridiemSwap ? 'pm' : 'am';
      }
      if (hour > 24) {
        hourDisplay = hour - 24;
      }
      for (let min = 0; min < 60; min += 30) {
        let minDisplay = min > 10 ? min : '0' + min;
        timeList.push({
          DisplayHour: hourDisplay + ':' + minDisplay + ' ' + meridiem
        });
      }
    }
    return timeList;
  }

  showStartTime() {
    if (this.ThisTime.value && !this.startTimeVisible) {
      this.startTimeVisible = !this.startTimeVisible;
      if (this.StartTime.value == undefined) {
        this.startTimeScrollTo('8:00 am');
      } else {
        this.startTimeScrollTo(this.StartTime.value);
      }
    }
  }
  showEndTime() {
    if (this.ThisTime.value && !this.endTimeVisible) {
      this.endTimeVisible = !this.endTimeVisible;
      if (this.EndTime.value == undefined) {
        this.endTimeScrollTo('9:00 am');
      } else {
        this.endTimeScrollTo(this.EndTime.value);
      }
    }
  }

  startTimeScrollTo(time: string) {
    setTimeout(() => {
      var container = document.getElementById('start-time-scroll');
      var rowToScrollTo = document.getElementById('start-' + time);
      if (rowToScrollTo !== undefined && rowToScrollTo !== null)
        container.scrollTop = rowToScrollTo.offsetTop;
    }, 10);
  }
  endTimeScrollTo(time: string) {
    setTimeout(() => {
      var container = document.getElementById('end-time-scroll');
      var rowToScrollTo = document.getElementById('end-' + time);
      if (rowToScrollTo !== undefined && rowToScrollTo !== null)
        container.scrollTop = rowToScrollTo.offsetTop;
    }, 10);
  }

  startTimeClickOutside(e: Event) {
    this.startTimeVisible = false;
  }
  endTimeClickOutside(e: Event) {
    this.endTimeVisible = false;
  }

  selectStartTime(time: any) {
    this.StartTime.setValue(time.DisplayHour);
    let splitEndTime = time.DisplayHour.split(':');
    let endTime;
    if (time.DisplayHour === '11:00 am') {
      endTime = '12:00 pm';
    } else if (time.DisplayHour === '11:00 pm') {
      endTime = '12:00 am';
    } else if (time.DisplayHour === '11:30 am') {
      endTime = '12:30 pm';
    } else if (time.DisplayHour === '11:30 pm') {
      endTime = '12:30 am';
    } else {
      endTime =
        (splitEndTime[0] === '12' ? '1' : parseInt(splitEndTime[0]) + 1) +
        ':' +
        splitEndTime[1];
    }
    this.EndTime.setValue(endTime);
    this.startTimeVisible = false;
  }
  selectEndTime(time: any) {
    this.EndTime.setValue(time.DisplayHour);
    this.endTimeVisible = false;
  }

}
