import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagePlaylistComponent } from './manage-playlist/manage-playlist.component';
import { IndexComponent } from './index/index.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
  },
  {
    path: 'manage-playlist',
    component: ManagePlaylistComponent
  },
  {
    path: 'manage-playlist/:id',
    component: ManagePlaylistComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
