import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { timer, Subscription } from 'rxjs';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.css']
})
export class StopwatchComponent implements OnChanges, OnInit, OnDestroy {
  @Input() ticks: number = 0;
  timerTicks: number = 0;
  secondsDisplay: number = 0;
  minutesDisplay: number = 0;
  hoursDisplay: number = 0;
  daysDisplay: number = 0;
  isPadZero: boolean = false;

  sub: Subscription;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.ticks) {
      this.timerTicks = 0;
      this.secondsDisplay = 0;
      this.minutesDisplay = 0;
      this.hoursDisplay = 0;
      this.daysDisplay = 0;
      this.startTimer();
    }
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  startTimer() {
    this.sub = timer(0, 1000)
      .subscribe(t => {
        this.timerTicks = this.ticks + t;
        this.secondsDisplay = this.getSeconds(this.timerTicks);
        this.minutesDisplay = this.getMinutes(this.timerTicks);
        this.hoursDisplay = this.getHours(this.timerTicks);
        this.daysDisplay = this.getDays(this.timerTicks);
      });
  }

  private getSeconds(ticks: number) {
    let seconds = ticks % 60;
    return this.isPadZero ? this.pad(seconds): seconds;
  }

  private getMinutes(ticks: number) {
    let minutes = (Math.floor(ticks / 60)) % 60;
    return this.isPadZero ? this.pad(minutes): minutes;
  }

  private getHours(ticks: number) {
    let hours = Math.floor((ticks / 60) / 60)  % 24;
    return this.isPadZero ? this.pad(hours): hours;
  }

  private getDays(ticks: number) {
    let days = Math.floor((ticks / 60) / 60 / 24);
    return this.isPadZero ? this.pad(days): days;
  }

  private pad(digit: any) {
    return digit <= 9 ? '0' + digit : digit;
  }
}
