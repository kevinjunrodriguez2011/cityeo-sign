import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit, OnChanges {
  @Input() hidePageSizeChanger: boolean;
  @Input() isUpDropdown: boolean;
  @Input() pageSize: number;
  @Input() totalRecords: number;
  @Input() currentPage: number;
  @Input() currentPageSize: number;
  @Output() onPageChanged = new EventEmitter();
  @Output() onPageSizeChanged = new EventEmitter();

  pager: any = {};
  pages: number[];
  pageSizes: number[];
  totalPages: number;

  constructor() { }

  ngOnChanges() {
    let pageSizeLength = 4;
    this.totalPages = Math.ceil(this.totalRecords / this.currentPageSize);

    if (this.currentPage < 1)
      this.currentPage = 1;
    else if (this.currentPage > this.totalRecords)
      this.currentPage = this.totalPages;

    //this.selectedPage = this.currentPage + 1;

    //generate page sizes for dropdown
    this.pageSizes = Array.from(Array(pageSizeLength).keys()).map(i => this.pageSize * (i + 1));
    //generate page numbers for dropdown
    this.pages = Array.from(Array(this.totalPages).keys()).map(i => i + 1);
    //this.pager = this.getPager(this.totalRecords, this.currentPage, this.currentPageSize);
  }

  ngOnInit() {

  }

  setPage(page: number) {
    if (this.currentPage !== page + 1) {
      this.currentPage = page + 1;
      this.onPageChanged.emit(page);
    }
  }

  setPageSize(pageSize: number) {
    if (this.currentPageSize !== pageSize) {
      this.currentPageSize = pageSize;
      this.currentPage = 1;
      this.onPageSizeChanged.emit(pageSize);
    }

  }

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    // calculate total pages
    let totalPages = Math.ceil(totalItems / pageSize);

    // ensure current page isn't out of range
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    // calculate start and end item indexes
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

    // create an array of pages to ng-repeat in the pager control
    let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage + i);

    // return object with all pager properties required by the view
    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

}
