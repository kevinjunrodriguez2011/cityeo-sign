import { NgModule } from "@angular/core";
import { PlaylistScheduleComponent } from "../playlists/playlist-schedule/playlist-schedule.component";
import { PlayerSetupComponent } from "../control-center/player-setup/player-setup.component";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { ClickOutsideModule } from "ng-click-outside";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
    imports:[
        CommonModule,
        ReactiveFormsModule,
        ClickOutsideModule,
        InfiniteScrollModule
    ],
    declarations: [
        PlaylistScheduleComponent,
        PlayerSetupComponent
    ],
    exports: [
        PlaylistScheduleComponent,
        PlayerSetupComponent
    ],
    providers:[
    ]
  })
  export class SharedModule { }