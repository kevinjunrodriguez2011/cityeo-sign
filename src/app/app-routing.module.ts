import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PermissionsComponent } from './views/permissions/permissions.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'media-library',
    pathMatch: 'full'
  },
  {
    path: 'media-library',
    loadChildren: 'src/app/views/media-library/media-library.module#MediaLibraryModule',
  },
  {
    path: 'devices',
    loadChildren: 'src/app/views/devices/devices.module#DevicesModule',
  },
  {
    path: 'control-center',
    loadChildren: 'src/app/views/control-center/control-center.module#ControlCenterModule',
  },
  {
    path: 'playlists',
    loadChildren: 'src/app/views/playlists/playlists.module#PlaylistsModule',
  },
  {
    path: 'permissions',
    component: PermissionsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
