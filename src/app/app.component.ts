import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
// import { MsalService } from 'angular-msal';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';
import { environment } from 'src/environments/environment';
import { NotifyEventService } from './services/notify-event.service';
import * as signalR from "@aspnet/signalr";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'cityeo-digital-sign';
  currentUrl = '';
  loggedIn: boolean;
  public isIframe: boolean;
  interval: any = 0;
  isRightSideBarOpened: boolean = false;
  private _hubConnection: signalR.HubConnection;

  constructor(private router: Router,
    private authService: AuthService,
    private notifyEventService: NotifyEventService) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl = event.url;
      }
    });

    //  This is to avoid reload during acquireTokenSilent() because of hidden iframe
    this.isIframe = window !== window.parent && !window.opener;
  }

  ngOnInit() {
    this.notifyEventService.supportSideBarEvent$.subscribe(res => {
      this.toggleRightSidebar();
    })

    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${environment.signalRHubUrl}/hubs/device?userId=1`)
      .build();

    this.startHubConnection();

    this._hubConnection.on('DeviceConnected', (deviceUniqueId: string) => {
      //callback when any device is disconnected from the hub
      this.notifyEventService.onDeviceConnectionStatusChange({deviceUniqueId: deviceUniqueId, IsOnline: true});
    });

    this._hubConnection.on('DeviceDisconnected', (deviceUniqueId: string) => {
      //callback when any device is disconnected from the hub
      this.notifyEventService.onDeviceConnectionStatusChange({deviceUniqueId: deviceUniqueId, IsOnline: false});
    });

    this._hubConnection.on('AutoPing', () => {
      console.log('auto ping');
      this._hubConnection.invoke('AutoPingReceived');
    });

    // this.getToken();

    // this.interval = setInterval(()=>{
    //   const foundToken = localStorage.getItem("msal.foundtoken");
    //   if (foundToken !== null) {
    //     this.loggedIn = false;

    //     if(foundToken === 'true'){
    //       this.loggedIn = true;
    //     }

    //     localStorage.removeItem('msal.foundtoken');
    //     clearInterval(this.interval);
    //   }
    //   if(this.loggedIn){
    //     localStorage.removeItem('msal.foundtoken');
    //     clearInterval(this.interval);
    //   }
    // }, 500);
  }

  startHubConnection() {
    this._hubConnection
    .start()
    .then(() => console.log('connection started!'))
    .catch(err => {
      console.log(err);
      this.startHubConnection();
    });
  }

  ngOnDestroy() {

  }

  isSideBarHidden() {
    return this.currentUrl.indexOf('/add-media') > -1
      || this.currentUrl.indexOf('/edit-media') > -1
      || this.currentUrl.indexOf('/manage-playlist') > -1;
  }

  login() {
    this.authService.login();
  }

  forgotPassword(){
    this.authService.forgotPassword();
  }

  logout() {
    this.authService.logout();
  }

  isOnline() {
    return this.authService.isOnline();
  }

  toggleRightSidebar() {
    this.isRightSideBarOpened = !this.isRightSideBarOpened;
  }
}