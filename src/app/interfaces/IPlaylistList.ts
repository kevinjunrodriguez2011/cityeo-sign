import { IPlaylistItem } from "./IPlaylistItem";

export interface IPlaylistList {
    Id: number;
    Name: string;
    IsEveryday: boolean;
    IsForFullDay: boolean;
    Days: string;
    StartTime: string;
    EndTime: string;
    StartTimeMinutes: number;
    EndTimeMinutes: number;
    IsPortrait: boolean;
    DisplaySize: any;
    TotalFileSize: number;
    PlayListItems: Array<IPlaylistItem>;
    ModifiedDatetime: Date;
    Checked: boolean;
  }