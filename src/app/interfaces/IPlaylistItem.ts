import { MediaLibrary } from "../models/MediaLibrary";

export interface IPlaylistItem{
    Id: number;
    UserPlayListId: number;
    MediaLibrary: MediaLibrary;
}