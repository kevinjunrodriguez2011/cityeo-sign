interface IDeviceInfo {
    DeviceName: string;
    DeviceAddress: string;
    City: string;
    State: string;
    Zip: string;
    SerialNumber: string;
    RegistrationKey: string;
    DeviceType: string;
    OSVersion: string;
    TimeZone: string;
    Playlist: string;
}