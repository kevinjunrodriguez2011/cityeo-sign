import { IPlaylistList } from "./IPlaylistList";

export interface IDeviceList {
    Id: number;
    Name: string;
    Photo: string;
    Thumbnail: string;
    Address: string;
    State: string;
    City: string;
    Zip: string;
    SerialNumber: string;
    RegistrationCode: string;
    DeviceType: string;
    OsVersion: string;
    TimezoneId: number;
    Status: string;
    UniqueId: string;
    IsOnline: boolean;
    OfflineDatetime?: Date;
    ModifiedDatetime: Date;
    Checked: boolean;
    Playlist: IPlaylistList;
  }