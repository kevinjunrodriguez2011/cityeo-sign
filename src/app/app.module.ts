import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SlickModule } from 'ngx-slick';
import { ClickOutsideModule } from 'ng-click-outside';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { SidebarModule } from 'ng-sidebar';

import { LayoutModule } from './views/layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { ModalTemplatesModule } from './views/modal-templates/modal-templates.module';


import { HighlightMenuDirective } from './directives/highlight-menu.directive';
import { ModalService } from './services/modal.service';
import { NotifyEventService } from './services/notify-event.service';
import { CommonFunctionsService } from './services/common-functions.service';
import { PlaylistService } from './services/playlist.service';
import { AuthService } from './services/auth.service';
import { MediaLibraryService } from './services/media-library.service';
import { AuthHttpInterceptor } from './services/auth.httpInterceptor';
import { AppComponent } from './app.component';
import { PermissionsComponent } from './views/permissions/permissions.component';
import { ControlPanelModalComponent } from './views/modal-templates/control-panel-modal/control-panel-modal.component';
import { PlaylistPreviewComponent } from './views/modal-templates/playlist-preview/playlist-preview.component';
import { PlaylistScheduleModalComponent } from './views/modal-templates/playlist-schedule-modal/playlist-schedule-modal.component';
import { PlayerSetupModalComponent } from './views/modal-templates/player-setup-modal/player-setup-modal.component';



export function loggerCallback(logLevel, message, piiEnabled) {
  if (message !== undefined && message != null) {
    if (message.indexOf('Fragment has id token') != -1
      || message.indexOf('Fragment has access token') != -1) {
      localStorage.setItem('msal.foundtoken', 'true');
    }
    else if (message.indexOf('AADB2C90077') != -1) {
      localStorage.setItem('msal.foundtoken', 'false');
    }
  }
  console.log("client logging" + message);
}

@NgModule({
  declarations: [
    AppComponent,
    HighlightMenuDirective,
    PermissionsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ClickOutsideModule,
    SlickModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    ToastrModule.forRoot(),
    SidebarModule.forRoot(),
    AppRoutingModule,
    LayoutModule,
    ModalTemplatesModule
  ],
  entryComponents: [
    ControlPanelModalComponent,
    PlaylistPreviewComponent,
    PlaylistScheduleModalComponent,
    PlayerSetupModalComponent
  ],
  providers: [
    ToastrService,
    NotifyEventService, 
    ModalService, 
    CommonFunctionsService,
    PlaylistService, 
    MediaLibraryService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
