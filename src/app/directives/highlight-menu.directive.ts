import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  selector: '[appHighlightMenu]'
})
export class HighlightMenuDirective {

  constructor(private elem: ElementRef, private renderer: Renderer2) { 
    
  }
  @HostListener('click') onClick() {
    this.highlight('active-menu');
  }
  private highlight(className: string) {
    //this.renderer.removeClass(this.elem.nativeElement, 'menu-highlight')
    $(this.elem.nativeElement).siblings().removeClass(className);
    this.renderer.addClass(this.elem.nativeElement, className)
  }
}
