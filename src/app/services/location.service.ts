import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiUrlsService } from './api-urls.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  getStates(searchTerm){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.LocationStateList}?searchTerm=${searchTerm}`,
      {headers}
      )
  }

  getTimezones(){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.LocationTimezoneList}`,
      {headers}
      )
  }
}
