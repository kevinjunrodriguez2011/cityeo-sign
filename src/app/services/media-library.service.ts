import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ApiUrlsService } from './api-urls.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MediaLibraryService {
  
  constructor(private http: HttpClient) { }

  GetMediaLibraryTypeList(){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.MediaLibraryTypeList}`,
      {headers}
      );
  }

  GetMediaLibraryList(mediaLibraryTypeId: number, pageStart:number, pageSize: number, searchTerm: string = '') {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.MediaLibraryList}?mediaLibraryTypeId=${mediaLibraryTypeId}&searchTerm=${searchTerm}&pageStart=${pageStart}&pageSize=${pageSize}`,
      {headers}
      )
  }

  GetMediaLibraryDetails(id: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.MediaLibraryDetails}?id=${id}`,
      {headers}
      )
  }

  UploadMedia(formData: any): any {
    //const headers = new HttpHeaders().set('Content-Type', 'multipart/form-data');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.MediaUpload}`,
      formData,
      { 
        //headers: headers,
        reportProgress: true,
        observe: 'events' 
      }
    ).pipe(map((event) => {

      switch (event.type) {

        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { status: 'progress', message: progress };

        case HttpEventType.Response:
          return event.body;
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));
  }

  RemoveMedia(ids: Array<number>): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.MediaRemove}`,
      ids,
      { headers: headers }
    )
  }
}
