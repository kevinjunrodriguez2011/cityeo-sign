import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiUrlsService } from './api-urls.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ControlCenterService {

  constructor(private http: HttpClient) { }
  
  getControlCenterList(pageStart: number, pageSize: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.ControlCenterList}?pageStart=${pageStart}&pageSize=${pageSize}`,
      {headers}
      )
  }

  getControlCenterDetails(id: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.ControlCenterDetails}?id=${id}`,
      {headers}
      )
  }
  
  saveControlCenterDetails(controlCenterSaveModel: any): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.ControlCenterSave}`,
      JSON.stringify(controlCenterSaveModel),
      { headers }
    );
  }

  removeControlCenter(id: number){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.ControlCenterRemove}`,
      id,
      { headers }
    );
  }

  updateDisplayOrder(ids: number[]): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.ControlCenterUpdateDisplayOrder}`,
      ids,
      { headers: headers }
    )
  }
  
}
