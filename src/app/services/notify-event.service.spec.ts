import { TestBed } from '@angular/core/testing';

import { NotifyEventService } from './notify-event.service';

describe('NotifyEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotifyEventService = TestBed.get(NotifyEventService);
    expect(service).toBeTruthy();
  });
});
