import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiUrlsService } from './api-urls.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  constructor(private http: HttpClient) { }

  
  setDeviceInfo() {

  }

  getDeviceList(pageStart:number, pageSize: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.DeviceList}?pageStart=${pageStart}&pageSize=${pageSize}`,
      {headers}
      )
  }

  getAvailableDeviceList(pageStart:number, pageSize: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.DeviceAvailableList}?pageStart=${pageStart}&pageSize=${pageSize}`,
      {headers}
      )
  }

  getDeviceDetails(id: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.DeviceDetails}?id=${id}`,
      {headers}
      )
  }

  getDeviceDetailsByCode(code: string) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.DeviceDetailsByCode}?code=${code}`,
      {headers}
      )
  }

  generateRegistrationCode() {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.DeviceGenerateRegistrationCode}`,
      {headers}
      )
  }

  removeDevice(ids: Array<number>): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.DeviceRemove}`,
      ids,
      { headers: headers }
    )
  }

  saveDevice(deviceSaveModel: any): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.DeviceSave}`,
      JSON.stringify(deviceSaveModel),
      { headers }
    );
  }

  verifyDevice(deviceVerifyModel: any): any{
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.DeviceVerify}`,
      JSON.stringify(deviceVerifyModel),
      { headers }
    );
  }

  rebootDevice(deviceUniqueId: string): any{
    
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.DeviceReboot}`,
      JSON.stringify(deviceUniqueId),
      { headers: headers }
    )
  }
}
