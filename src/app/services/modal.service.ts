import { Injectable, Component, TemplateRef } from '@angular/core';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(private modalService: BsModalService) { }

  openModal(content: any, config: ModalOptions)  {
    config.backdrop = true;
    config.ignoreBackdropClick = true;
    return this.modalService.show(content, config);
  }
}
