import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ApiUrlsService } from './api-urls.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class PlaylistService {

  public selectedMedias = [];

  constructor(private http: HttpClient) { }

  getDisplaySizeList() {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.ControlCenterDisplaySizeList}`,
      {headers}
      )
  }
  
  getPlaylists(pageStart: number, pageSize: number, searchTerm: string = '') {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.PlaylistList}?searchTerm=${searchTerm}&pageStart=${pageStart}&pageSize=${pageSize}`,
      {headers}
      )
  }

  searchPlaylist(searchTerm: string = '', pageSize: number = 25){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.PlaylistSearch}?searchTerm=${searchTerm}&pageSize=${pageSize}`,
      {headers}
      )
  }
  
  getPlaylistDetails(id: number) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.PlaylistDetails}?id=${id}`,
      {headers}
      )
  }

  RemovePlayList(ids: number[]): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.PlaylistRemove}`,
      ids,
      { headers: headers }
    )
  }

  SavePlayList(playListSaveModel: any): any {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.PlaylistSave}`,
      JSON.stringify(playListSaveModel),
      { headers }
    );
  }
  
  GetPlayListSchedule(Id: number){
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(
      `${environment.webApi}${ApiUrlsService.PlaylistScheduleGet}?Id=${Id}`,
      {headers}
      )
  }

  SetPlayListSchedule(playListScheduleSaveModel: any): any{
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(
      `${environment.webApi}${ApiUrlsService.PlaylistScheduleSet}`,
      JSON.stringify(playListScheduleSaveModel),
      { headers }
    );
  }

  selectMedia(media) {
    media.Selected = true;
    if (this.selectedMedias.filter(c => c.Id === media.Id).length > 0) {
      media.Selected = false;
      var mediaIndex = this.selectedMedias.map(c => c.Id).indexOf(media.Id)
      this.selectedMedias.splice(mediaIndex, 1)
      return;
    }
    this.selectedMedias.push({...media});
  }
}
