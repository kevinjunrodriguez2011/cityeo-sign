export class ApiUrlsService {
    private static MediaLibraryPrefix = 'MediaLibrary';
    private static PlaylistPrefix = 'Playlist';
    private static DevicePrefix = 'Device';
    private static LocationPrefix = 'Location';
    private static ControlCenterPrefix = 'ControlCenter';

    static MediaLibraryTypeList = `/${ApiUrlsService.MediaLibraryPrefix}/GetMediaLibraryTypeList`;
    static MediaLibraryList = `/${ApiUrlsService.MediaLibraryPrefix}/GetMediaLibraryList`;
    static MediaLibraryDetails= `/${ApiUrlsService.MediaLibraryPrefix}/GetMediaLibraryDetails`;
    static MediaUpload = `/${ApiUrlsService.MediaLibraryPrefix}/UploadMedia`;
    static MediaRemove = `/${ApiUrlsService.MediaLibraryPrefix}/RemoveMedia`;

    static PlaylistList = `/${ApiUrlsService.PlaylistPrefix}/GetPlayLists`;
    static PlaylistSearch = `/${ApiUrlsService.PlaylistPrefix}/SearchPlaylist`;
    static PlaylistDetails= `/${ApiUrlsService.PlaylistPrefix}/GetPlayListDetails`;
    static PlaylistSave = `/${ApiUrlsService.PlaylistPrefix}/SavePlayList`;
    static PlaylistRemove = `/${ApiUrlsService.PlaylistPrefix}/RemovePlaylist`;
    static PlaylistScheduleGet = `/${ApiUrlsService.PlaylistPrefix}/GetSchedule`;
    static PlaylistScheduleSet = `/${ApiUrlsService.PlaylistPrefix}/SetSchedule`;
 
    static LocationStateList = `/${ApiUrlsService.LocationPrefix}/GetStates`;
    static LocationTimezoneList = `/${ApiUrlsService.LocationPrefix}/GetTimezones`;

    static DeviceList = `/${ApiUrlsService.DevicePrefix}/GetDevices`;
    static DeviceAvailableList = `/${ApiUrlsService.DevicePrefix}/GetAvailableDevices`;
    static DeviceDetails = `/${ApiUrlsService.DevicePrefix}/GetDeviceDetails`;
    static DeviceGenerateRegistrationCode= `/${ApiUrlsService.DevicePrefix}/GenerateRegistrationCode`;
    static DeviceDetailsByCode = `/${ApiUrlsService.DevicePrefix}/GetDeviceDetailsByCode`;
    static DeviceSave = `/${ApiUrlsService.DevicePrefix}/SaveDevice`;
    static DeviceRemove = `/${ApiUrlsService.DevicePrefix}/RemoveDevice`;
    static DeviceVerify = `/${ApiUrlsService.DevicePrefix}/Verify`;
    static DeviceReboot = `/${ApiUrlsService.DevicePrefix}/RebootDevice`;
 
    static ControlCenterList = `/${ApiUrlsService.ControlCenterPrefix}/GetControlCenterDeviceList`;
    static ControlCenterDetails = `/${ApiUrlsService.ControlCenterPrefix}/GetControlCenterDeviceDetails`;
    static ControlCenterDisplaySizeList = `/${ApiUrlsService.ControlCenterPrefix}/GetDisplaySizeList`;
    static ControlCenterSave = `/${ApiUrlsService.ControlCenterPrefix}/SaveControlCenterDetails`;
    static ControlCenterRemove = `/${ApiUrlsService.ControlCenterPrefix}/RemoveControlCenterDetails`;
    static ControlCenterUpdateDisplayOrder = `/${ApiUrlsService.ControlCenterPrefix}/UpdateDeviceDisplayOrder`;
}