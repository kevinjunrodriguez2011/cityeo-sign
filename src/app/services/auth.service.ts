import * as Msal from 'angular-msal/node_modules/msal';
import { environment } from "src/environments/environment";
import { ApiUrlsService } from './api-urls.service';


export class AuthService {
  private tenant: string = environment.tenant;
  private tenantName = this.tenant.substr(0, this.tenant.indexOf('.'));
  //private authority = 'https://login.microsoftonline.com/tfp/' + environment.tenant + '/' + environment.signUpSignInPolicy;
  private authority = `https://${this.tenantName}.b2clogin.com/${this.tenant}/${environment.signUpSignInPolicy}`;
  private clientApplication: Msal.UserAgentApplication;

  readWriteB2cScopes = environment.b2cScopes.concat(environment.extraB2cScopes)

  // protectedResourceMap: Map<string, string[]> = new Map([
  //   [environment.webApi + ApiUrlsService.MediaLibraryList, environment.b2cScopes]
  //   , [environment.webApi + ApiUrlsService.PlaylistList, environment.b2cScopes]
  //   , [environment.webApi + ApiUrlsService.PlaylistSave, this.readWriteB2cScopes]
  // ]);

  protectedResourceMap: Map<string, string[]> = new Map([
  ]);

  constructor() {

    let config: Msal.Configuration = {
      auth: {
        authority: this.authority,
        clientId: environment.clientID,
        validateAuthority: false
      },
      cache: {
        cacheLocation: 'localStorage',
        storeAuthStateInCookie: true
      }
    }
    this.clientApplication = new Msal.UserAgentApplication(config);

    this.clientApplication.handleRedirectCallback((response: Msal.AuthResponse) => {
      console.log('token received callback');
    }, (authErr: Msal.AuthError, accountState: string) => {
      console.log('auth error callback');
      console.log(authErr)
      console.log(accountState)
      if(authErr.errorMessage.indexOf('AADB2C90118') > -1) {
        this.forgotPassword();
      }
    });
  }
  
  public login(): void {
    this.clientApplication.authority = this.authority;

    this.clientApplication.loginRedirect({
      scopes: environment.b2cScopes,
    });
  }

  public forgotPassword(): void{
    let authority = this.authority.substring(0, this.authority.lastIndexOf('/') + 1) + environment.passwordResetPolicy;    
    this.clientApplication.authority = authority;

    this.clientApplication.loginRedirect({
      scopes: environment.b2cScopes,
    });
  }

  public logout(): void {
    this.clientApplication.logout();
  }

  public isOnline(): boolean {
    return this.clientApplication.getAccount() != null;
  }

  public getUser() {
    return this.clientApplication.getAccount();
  }

  getScopesForEndpoint(endpoint: string): string[] {
    // process all protected resources and send the matched one
    if (this.protectedResourceMap.size > 0) {
      for (var i = 0, _a = Array.from(this.protectedResourceMap.keys()); i < _a.length; i++) {
        var key = _a[i];
        if (endpoint != null && key != null
          && endpoint.toLowerCase().indexOf(key.toLowerCase()) > -1) {
          return this.protectedResourceMap.get(key);
        }
      }
    }

    return null;
  }

  public getAuthenticationToken(scopes: string[]): Promise<string> {
    return this.clientApplication.acquireTokenSilent({ scopes: scopes })
      .then(token => {
        console.log('Got silent access token: ', token.accessToken);
        return token.accessToken;
      }).catch(error => {
        console.log('Could not silently retrieve token from storage.', error);
        //this.clientApplication.acquireTokenRedirect();
        return Promise.resolve('');
        // return this.clientApplication.acquireTokenPopup({ scopes: environment.b2cScopes })
        //   .then(token => {
        //     console.log('Got popup access token: ', token.accessToken);
        //     return Promise.resolve(token.accessToken);
        //   }).catch(innererror => {
        //     console.log('Could not retrieve token from popup.', innererror);
        //     return Promise.resolve('');
        //   });
      });
  }
}