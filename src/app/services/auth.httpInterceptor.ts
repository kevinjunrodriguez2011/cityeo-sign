import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { tap, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

@Injectable()
export class AuthHttpInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const scopes = this.authService.getScopesForEndpoint(req.url);
        if (scopes === null) {
            return next.handle(req);
        }

        return from(this.authService.getAuthenticationToken(scopes))
            .pipe(switchMap(token => {

                req = req.clone({
                    setHeaders: {
                        Authorization: `Bearer ${token}`
                    }
                });

                return next.handle(req)
                    .pipe(tap(nextOrObserver => {

                    }, error => {
                        if (error instanceof HttpErrorResponse && error.status === 401) {
                            //this.authService.login();
                        }
                    }));
            }));
    }
}
