import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs'

@Injectable()
export class NotifyEventService {

  private managePlaylistEvent = new Subject<any>();
  managePlaylistEvent$ = this.managePlaylistEvent.asObservable();

  private addMediaEvent = new Subject<any>();
  addMediaEvent$ = this.addMediaEvent.asObservable();

  managePlaylistName = '';

  private newPlaylistEvent = new Subject<any>();
  newPlaylistEvent$ = this.newPlaylistEvent.asObservable();

  private mediaLibrarySearchTermEvent = new Subject<any>();
  mediaLibrarySearchTermEvent$ = this.mediaLibrarySearchTermEvent.asObservable();

  private playlistSetScheduleEvent = new Subject<any>();
  playlistSetScheduleEvent$ = this.playlistSetScheduleEvent.asObservable();

  private playerSetupEvent = new Subject<any>();
  playerSetupEvent$ = this.playerSetupEvent.asObservable();

  private supportSideBarEvent = new Subject<any>();
  supportSideBarEvent$ = this.supportSideBarEvent.asObservable();

  private deviceConnectionStatusChangeEvent = new Subject<any>();
  deviceConnectionStatusChangeEvent$ = this.deviceConnectionStatusChangeEvent.asObservable();

  constructor() { }

  public onManagePlaylist(playlistName) {
    this.managePlaylistEvent.next(playlistName);
  }
  public onModalAddMedia(medias) {
    this.addMediaEvent.next(medias);
  }
  public onNewPlaylist() {
    this.newPlaylistEvent.next();
  }

  public onMediaLibrarySearchTerm(searchTerm){
    this.mediaLibrarySearchTermEvent.next(searchTerm);
  }

  public onPlaylistSetSchedule(playListScheduleId){
    this.playlistSetScheduleEvent.next(playListScheduleId);
  }

  public onPlayerSetup(isSaved){
    this.playerSetupEvent.next(isSaved);
  }

  public onSupportSideBarToggle(){
    this.supportSideBarEvent.next();
  }

  public onDeviceConnectionStatusChange(deviceConectionStatus: Object){
    this.deviceConnectionStatusChangeEvent.next(deviceConectionStatus);
  }
}
