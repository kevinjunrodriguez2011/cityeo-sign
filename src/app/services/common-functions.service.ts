import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";

@Injectable({
  providedIn: "root"
})
export class CommonFunctionsService {
  fileSizeUnit: number = 1024;
  maxFileSize: number = 50 * this.fileSizeUnit * this.fileSizeUnit;//50MB
  defaultMediaDuration: number = 5;
  playlistScheuleDaysShort = ['M', 'Tu', 'W', 'Th', 'F', 'Sa', 'Su'];
  playlistScheuleDaysLong = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  playlistScheuleDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  defaultErrorMessage: string = 'Something went wrong. Please try again soon.';

  constructor(private toastrService: ToastrService) {

  }

  getFileSizeOnHighUnit(fileSize: number): number {
    if (fileSize > 0) {
      if (fileSize < this.fileSizeUnit * this.fileSizeUnit) {
        fileSize = parseFloat((fileSize / this.fileSizeUnit).toFixed(2))
      }
      else if (fileSize < this.fileSizeUnit * this.fileSizeUnit * this.fileSizeUnit) {
        fileSize = parseFloat((fileSize / this.fileSizeUnit / this.fileSizeUnit).toFixed(2))
      }
    }

    return fileSize;
  }

  getFileSizeUnit(fileSize: number) {
    let fileSizeInWords = 'bytes';

    if (fileSize > 0) {
      if (fileSize < this.fileSizeUnit) {
        fileSizeInWords = 'bytes'
      }
      else if (fileSize < this.fileSizeUnit * this.fileSizeUnit) {
        fileSizeInWords = 'KB'
      }
      else if (fileSize < this.fileSizeUnit * this.fileSizeUnit * this.fileSizeUnit) {
        fileSizeInWords = 'MB'
      }
    }

    return fileSizeInWords;
  }

  validateFileType(fileType: string) {
    return this.validateImageFileType(fileType) || fileType.indexOf('video') > -1;
  }

  validateFileSize(fileSize: number) {
    return fileSize <= this.maxFileSize;
  }

  validateImageFileType(fileType: string) {
    return fileType === 'image/jpeg' || fileType === 'image/png';
  }

  formatFileSize(fileSize: number): string {
    let finalFileSize = '0 bytes';
    let fileSizeInWords = '';

    if (fileSize > 0) {
      if (fileSize < this.fileSizeUnit) {
        fileSizeInWords = 'bytes'
      }
      if (fileSize < this.fileSizeUnit * this.fileSizeUnit) {
        fileSize = parseFloat((fileSize / this.fileSizeUnit).toFixed(2))
        fileSizeInWords = 'KB'
      }
      else if (fileSize < this.fileSizeUnit * this.fileSizeUnit * this.fileSizeUnit) {
        fileSize = parseFloat((fileSize / this.fileSizeUnit / this.fileSizeUnit).toFixed(2))
        fileSizeInWords = 'MB'
      }
      finalFileSize = fileSize + ' ' + fileSizeInWords;
    }

    return finalFileSize;
  }

  formatDuration(totalSeconds: number, isIncludeHours = false) {
    let durationString: string = '';

    let hours = Math.floor(totalSeconds / 3600);
    totalSeconds = totalSeconds % 3600;
    let minutes = Math.floor(totalSeconds / 60);
    let seconds = totalSeconds % 60;

    if (isIncludeHours)
      durationString = `${hours < 10 ? `0${hours}` : hours}:`;

    durationString += `${minutes < 10 ? `0${minutes}` : minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;

    return durationString;
  }

  formatTimeAMPM(minutes: number) {
    let finalTimeString: string = '';
    if (minutes !== undefined && minutes != null) {
      let strAmPM = 'am';
      let date = new Date();
      date.setHours(0, minutes, 0, 0);
      let finalHours = date.getHours();
      strAmPM = (finalHours >= 12 ? 'pm' : 'am');

      // let hours = parseInt(String(minutes / 60));

      // let finalHours = (hours - (hours >= 24 ? 24 : 0));
      finalHours = (finalHours - (finalHours > 12 ? 12 : 0));
      finalHours = (finalHours == 0 ? 12 : finalHours);
      let finalMinutes = minutes % 60;

      finalTimeString = String(finalHours);
      finalTimeString += ":" + (finalMinutes < 10 ? "0" + finalMinutes : String(finalMinutes));
      //finalTimeString += " " + ((finalHours >= 12 && finalHours < 24) ? "pm" : "am");
      finalTimeString += " " + strAmPM;
    }

    return finalTimeString;
  }

  formatDaysFromNumbers(days: string) {
    let arrDayNames: string[] = [];
    if(days !== undefined && days != null){
      let arrdays = days.split(',');
      arrdays.forEach(element => {
        if (parseInt(element) >= 1 && parseInt(element) <= 7) {
          arrDayNames.push(this.playlistScheuleDaysShort[parseInt(element) - 1]);
        }
      });
    }
    return arrDayNames.join(', ');
  }

  displaySuccessMessage(message: string, isShowTitle: boolean = false) {
    let defaultTitle = 'SUCCESS!';
    this.toastrService.success(message, isShowTitle ? defaultTitle: null, {
      timeOut: 4000,
      positionClass: 'toast-top-right',
      tapToDismiss: true,
      easing: 'ease-in-out',
      easeTime: 400,
      closeButton: true
    });
  }

  displayErrorMessage(message: string = this.defaultErrorMessage, isShowTitle: boolean = false) {
    let defaultTitle = 'ERROR!';

    if (message == null || message.length == 0)
      message = this.defaultErrorMessage;

    this.toastrService.error(message, isShowTitle ? defaultTitle: null, {
      timeOut: 4000,
      positionClass: 'toast-top-right',
      tapToDismiss: true,
      easing: 'ease-in-out',
      easeTime: 400,
      closeButton: true
    });
  }
}