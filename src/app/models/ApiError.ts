export class ApiError{
    error_description: string;
    error_type: string;
    data: Object;
}