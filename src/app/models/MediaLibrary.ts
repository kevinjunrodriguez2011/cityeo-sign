export class MediaLibrary{
    Id: number;
    MediaLibraryTypeId: number;
    Title: string;
    Url: string;
    ThumbnailUrl: string;
    FrameWidth: number;
    FrameHeight: number;
    Duration: number;
    FileSize: number;
    MIMEType: string;
    CreatedDatetime: Date;
    Selected: boolean;
}